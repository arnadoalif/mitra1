<!-- for bootstrap working -->
	<script src="<?php echo base_url('asset_front/js/bootstrap.js'); ?> "></script>
	<!-- flexSlider -->
		<script defer src="<?php echo base_url('asset_front/js/jquery.flexslider.js'); ?> "></script>
		<script type="text/javascript">
		$(window).load(function(){
		  $('.flexslider').flexslider({
			animation: "slide",
			start: function(slider){
			  $('body').removeClass('loading');
			}
		  });
		});
	  </script>
	<!-- //flexSlider -->
<!-- //for bootstrap working -->