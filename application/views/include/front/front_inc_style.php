<title>Guyubingmanah</title>
<!-- custom-theme -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<link rel="icon" href="<?php echo base_url('asset_front/images/logo_guyup.png') ?>">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //custom-theme -->
<link href="<?php echo base_url('asset_front/css/bootstrap.css'); ?> " rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url('asset_front/css/style.css'); ?> " rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script type="text/javascript" src="<?php echo base_url('asset_front/js/jquery-2.1.4.min.js'); ?> "></script>
<!-- //js -->
<link rel="stylesheet" href="<?php echo base_url('asset_front/css/flexslider.css'); ?> " type="text/css" media="screen" property="" />
<!-- font-awesome-icons -->
<link href="<?php echo base_url('asset_front/css/font-awesome.css'); ?> " rel="stylesheet"> 
<!-- //font-awesome-icons -->
<link href="//fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>