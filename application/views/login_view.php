<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Login Admin</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url('asset_admin/vendors/bootstrap/dist/css/bootstrap.min.css'); ?> " rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url('asset_admin/vendors/font-awesome/css/font-awesome.min.css'); ?> " rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url('asset_admin/vendors/nprogress/nprogress.css'); ?> " rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo base_url('asset_admin/vendors/animate.css/animate.min.css'); ?> " rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url('asset_admin/build/css/custom.min.css'); ?> " rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <?php if (isset($_SESSION['message_data'])): ?>
            <div class="alert alert-success" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
              </button>
              <?php echo $_SESSION['message_data'] ?>
            </div>
            <?php endif ?>

            <?php if (isset($_SESSION['error_data'])): ?>
            <div class="alert alert-danger" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
              </button>
              <?php echo $_SESSION['error_data'] ?>
            </div>
          <?php endif ?>
          <section class="login_content">
            <form action="<?php echo base_url('login/action_login'); ?>" method="POST">
              <h1>Login Form</h1>
              <div>
                <input type="text" name="username" class="form-control" placeholder="Username" required="" />
              </div>
              <div>
                <input type="password" name="password" class="form-control" placeholder="Password" required="" />
              </div>
              <button type="submit" class="btn btn-block btn-success"><i class="fa fa-sign-in"></i> Login</button>
              <div>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <div class="clearfix"></div>
                <br />

                <div>
                  <p>©2018 Guyup Bing Mana | WebDev PT Peksigunaraharja</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>
