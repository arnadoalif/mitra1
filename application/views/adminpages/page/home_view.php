<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/inc_style.php'); ?>
    <style type="text/css">
      .tile-stats .count {
          font-size: 25px;
          font-weight: 700;
          line-height: 2.65857;
      }

      .tile-stats h3 {
          color: #BAB8B8;
          font-size: 16px;
      }

      .fc-unthemed td.fc-today {
        background: #4d430c;
      }

      .fc-sun {
        background: red;
      }

      .fc-ltr .fc-basic-view .fc-day-top .fc-day-number {
        float: right;
        color: #000;
      }

      .fc-day-header .fc-sun {
        color: #fff;
      }

      .green {
          color: #548d0c;
          font-weight: bold;
      }

      .red {
        color: #d71602;
        font-weight: bold;
      }
    </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php $this->load->view('adminpages/main_menu'); ?>

        <!-- top navigation -->
        <?php $this->load->view('adminpages/nav_bar'); ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <h3>Periode Sekarang <b><?php echo $data_periode; ?></b></h3>

            <div class="col-md-7 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2></h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <p class="text-muted font-13 m-b-30">
                  </p>
                  <table id="datatable-fixed-header" class="table table-striped table-bordered" style="color: #000;">
                    <thead>
                      <tr>
                        <th>Nama</th>
                        <th>Periode Sekarang <?php echo $data_periode; ?></th>
                        <th>Periode Lalu <?php echo $data_periode_last ?></th>
                        <th>Presentase</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Jumlah Plasma</td>
                        <td class="count" id="jum_plasma" data-id="<?php echo $data_populasi->JumPlas; ?>"></td>
                        <td><?php echo number_format($data_populasi_last->JumPlas,0,'','.'); ?></td>
                        <td>
                          <?php if ($data_populasi->JumPlas >= $data_populasi_last->JumPlas): ?>
                            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>
                              <?php echo ceil((($data_populasi->JumPlas - $data_populasi_last->JumPlas)/$data_populasi_last->JumPlas*100))."%"; ?> 
                              </i>  <span class="hidden-xs"></span> </span>
                          <?php else: ?>
                            <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo ceil((($data_populasi->JumPlas - $data_populasi_last->JumPlas)/$data_populasi_last->JumPlas*100))."%"; ?> </i> </span>
                          <?php endif ?>
                        </td>
                      </tr>
                      <tr>
                        <td>Jumlah Populasi</td>
                        <td class="count" id="populasi" data-id="<?php echo $data_populasi->Populasi ?>"></td>
                        <td><?php echo number_format($data_populasi_last->Populasi,0,'','.'); ?></td>
                        <td>
                          <?php if ($data_populasi->Populasi >= $data_populasi_last->Populasi): ?>
                            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>
                              <?php echo ceil((($data_populasi->Populasi - $data_populasi_last->Populasi)/$data_populasi_last->Populasi*100))."%"; ?> 
                              </i> </span>
                          <?php else: ?>
                            <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo ceil((($data_populasi->Populasi - $data_populasi_last->Populasi)/$data_populasi_last->Populasi*100))."%"; ?> </i> </span>
                          <?php endif ?>
                        </td>
                      </tr>
                      <tr>
                        <td>Jumlah Surplus</td>
                        <td class="count" id="surplus" data-id="<?php echo$data_populasi->Surplus ?>"></td>
                        <td><?php echo number_format($data_populasi_last->Surplus,0,'','.'); ?></td>
                        <td>
                          <?php if ($data_populasi->Surplus >= $data_populasi_last->Surplus): ?>
                            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>
                              <?php echo ceil((($data_populasi->Surplus - $data_populasi_last->Surplus)/$data_populasi_last->Surplus*100))."%"; ?> 
                              </i> </span>
                          <?php else: ?>
                            <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo ceil((($data_populasi->Surplus - $data_populasi_last->Surplus)/$data_populasi_last->Surplus*100))."%"; ?> </i> </span>
                          <?php endif ?>
                        </td>
                      </tr>
                      <tr>
                        <td>Jumlah Tabungan</td>
                        <td class="count" id="tabungan" data-id="<?php echo $data_populasi->Tabungan ?>"></td>
                        <td><?php echo number_format($data_populasi_last->Tabungan,0,'','.'); ?></td>
                        <td>
                          <?php if ($data_populasi->Tabungan >= $data_populasi_last->Tabungan): ?>
                            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>
                              <?php echo ceil((($data_populasi->Tabungan - $data_populasi_last->Tabungan)/$data_populasi_last->Tabungan*100))."%"; ?> 
                              </i> </span>
                          <?php else: ?>
                            <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo ceil((($data_populasi->Tabungan - $data_populasi_last->Tabungan)/$data_populasi_last->Tabungan*100))."%"; ?> </i> </span>
                          <?php endif ?>
                        </td>
                      </tr>
                      <tr>
                        <td>Jumlah Rata - Rata Surplus</td>
                        <td class="count" id="rata-rata" data-id="<?php echo $data_populasi->RataSurplus; ?>"></td>
                        <td><?php echo number_format($data_populasi_last->RataSurplus,0,'','.'); ?></td>
                        <td>
                          <?php if ($data_populasi->RataSurplus >= $data_populasi_last->RataSurplus): ?>
                            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>
                              <?php echo ceil((($data_populasi->RataSurplus - $data_populasi_last->RataSurplus)/$data_populasi_last->RataSurplus*100))."%"; ?> 
                              </i> </span>
                          <?php else: ?>
                            <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo ceil((($data_populasi->RataSurplus - $data_populasi_last->RataSurplus)/$data_populasi_last->RataSurplus*100))."%"; ?> </i> </span>
                          <?php endif ?>
                        </td>
                      </tr>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>

            <div class="col-md-5 col-sm-12 col-xs-12">
              <h2 class="hidden-lg hidden-md">Kalender Periode Selanjutnya <?php echo $data_periode+1; ?></h2>
              <div class="clearfix"></div>
              <div class="row">
                <div class="col-md-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2 class="hidden-xs hidden-sm">Kalender Periode Selanjutnya <?php echo $data_periode+1; ?></h2>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                      <div id='data-calendar'></div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
      

          <br />
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php $this->load->view('adminpages/footer'); ?>
        <!-- /footer content -->
      </div>
    </div>

    <?php require_once(APPPATH .'views/include/admin/inc_script.php'); ?>
    <script>
      var d = new Date();
      var n = d.getMonth();
      var y = d.getFullYear();
          // Y        M      D
      var date = y+'-'+(n+1)+'-'+10;

      $(document).ready(function() {
        var base_url = window.location.origin;
        $('#data-calendar').fullCalendar({
          monthNames: ['Januari','Febuary','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'],
            monthNamesShort: ['Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agu','Sep','Okt','Nov','Des'],
            dayNames: ['Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu'],
            dayNamesShort: ['Min','Sen','Sel','Rab','Kam','Jum','Sab'],
            weekNumbers: true,
            
          defaultDate: date,
          editable: false,
          eventLimit: "more", // allow "more" link when too many events
          events:[]
        });
      });
    </script>

    <script type="text/javascript">
      jQuery(document).ready(function($) {
        var jum_plasma = $('#jum_plasma').attr("data-id");
        var populasi = $('#populasi').attr("data-id");
        var surplus = $('#surplus').attr("data-id");
        var tabungan = $('#tabungan').attr("data-id");
        var ratarata = $('#rata-rata').attr("data-id");

        var options = {
          useEasing: true, 
          useGrouping: true, 
          separator: '.', 
          decimal: '.', 
        };
        var jum_plasma = new CountUp(document.getElementById("jum_plasma"), 0, jum_plasma, 0, 2.5, options);
        var populasi = new CountUp(document.getElementById("populasi"), 0, populasi, 0, 2.5, options);
        var surplus = new CountUp(document.getElementById("surplus"), 0, surplus, 0, 2.5, options);
        var tabungan = new CountUp(document.getElementById("tabungan"), 0, tabungan, 0, 2.5, options);
        var ratarata = new CountUp(document.getElementById("rata-rata"), 0, ratarata, 0, 2.5, options);
        jum_plasma.start();
        populasi.start();
        surplus.start();
        tabungan.start();
        ratarata.start();
        
      });
      
    </script>
	
  </body>
</html>
