<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/inc_style.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php $this->load->view('adminpages/main_menu'); ?>

        <!-- top navigation -->
        <?php $this->load->view('adminpages/nav_bar'); ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="x_panel">
                <div class="x_title">
                  <h2></h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <p class="text-muted font-13 m-b-30">
                  </p>
                  <table id="datatable-fixed-header" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Koordinator</th>
                        <th>Nama</th>
                        <th>Periode Sekarang <?php echo $data_periode; ?></th>
                        <th>Periode Lalu <?php echo $data_periode_last ?></th>
                        <th>Presentase</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td rowspan="5" align="center">001 Nanang Brosot</td>
                        <td>Jumlah Plasma</td>
                        <td><?php echo $data_populasi_001->JumPlas; ?></td>
                        <td><?php echo  $data_populasi_last_001->JumPlas; ?></td>
                        <td>
                          <?php if ($data_populasi_001->JumPlas >= $data_populasi_last_001->JumPlas): ?>
                            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>
                              <?php echo ceil((($data_populasi_001->JumPlas - $data_populasi_last_001->JumPlas)/$data_populasi_last_001->JumPlas*100))."%"; ?> 
                              </i> Dari yang lalu</span>
                          <?php else: ?>
                            <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo ceil((($data_populasi_001->JumPlas - $data_populasi_last_001->JumPlas)/$data_populasi_last_001->JumPlas*100))."%"; ?> </i> Dari yang lalu</span>
                          <?php endif ?>
                        </td>
                      </tr>
                      <tr>
                        <td>Jumlah Populasi</td>
                        <td><?php echo number_format($data_populasi_001->Populasi ,0,'','.') ?></td>
                        <td><?php echo number_format($data_populasi_last_001->Populasi ,0,'','.') ?></td>
                        <td>
                          <?php if ($data_populasi_001->Populasi >= $data_populasi_last_001->Populasi): ?>
                            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>
                              <?php echo ceil((($data_populasi_001->Populasi - $data_populasi_last_001->Populasi)/$data_populasi_last_001->Populasi*100))."%"; ?> 
                              </i> Dari yang lalu</span>
                          <?php else: ?>
                            <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo ceil((($data_populasi_001->Populasi - $data_populasi_last_001->Populasi)/$data_populasi_last_001->Populasi*100))."%"; ?> </i> Dari yang lalu</span>
                          <?php endif ?>
                        </td>
                      </tr>
                      <tr>
                        <td>Jumlah Surplus</td>
                        <td><?php echo number_format($data_populasi_001->Surplus, 0,'','.') ?></td>
                        <td><?php echo number_format($data_populasi_last_001->Surplus, 0,'','.') ?></td>
                        <td>
                          <?php if ($data_populasi_001->Surplus >= $data_populasi_last_001->Surplus): ?>
                            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>
                              <?php echo ceil((($data_populasi_001->Surplus - $data_populasi_last_001->Surplus)/$data_populasi_last_001->Surplus*100))."%"; ?> 
                              </i> Dari yang lalu</span>
                          <?php else: ?>
                            <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo ceil((($data_populasi_001->Surplus - $data_populasi_last_001->Surplus)/$data_populasi_last_001->Surplus*100))."%"; ?> </i> Dari yang lalu</span>
                          <?php endif ?>
                        </td>
                      </tr>
                      <tr>
                        <td>Jumlah Tabungan</td>
                        <td><?php echo number_format($data_populasi_001->Tabungan, 0,'','.') ?></td>
                        <td><?php echo number_format($data_populasi_last_001->Tabungan, 0,'','.') ?></td>
                        <td>
                          <?php if ($data_populasi_001->Tabungan >= $data_populasi_last_001->Tabungan): ?>
                            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>
                              <?php echo ceil((($data_populasi_001->Tabungan - $data_populasi_last_001->Tabungan)/$data_populasi_last_001->Tabungan*100))."%"; ?> 
                              </i> Dari yang lalu</span>
                          <?php else: ?>
                            <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo ceil((($data_populasi_001->Tabungan - $data_populasi_last_001->Tabungan)/$data_populasi_last_001->Tabungan*100))."%"; ?> </i> Dari yang lalu</span>
                          <?php endif ?>
                        </td>
                      </tr>
                      <tr>
                        <td>Rata - rata surplus</td>
                        <td><?php echo number_format($data_populasi_001->RataSurplus,0,'','.'); ?></td>
                        <td><?php echo number_format($data_populasi_last_001->RataSurplus,0,'','.'); ?></td>
                        <td>
                          <?php if ($data_populasi_001->RataSurplus >= $data_populasi_last_001->RataSurplus): ?>
                            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>
                              <?php echo ceil((($data_populasi_001->RataSurplus - $data_populasi_last_001->RataSurplus)/$data_populasi_last_001->RataSurplus*100))."%"; ?> 
                              </i> Dari yang lalu</span>
                          <?php else: ?>
                            <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo ceil((($data_populasi_001->RataSurplus - $data_populasi_last_001->RataSurplus)/$data_populasi_last_001->RataSurplus*100))."%"; ?> </i> Dari yang lalu</span>
                          <?php endif ?>
                        </td>
                      </tr>
                    </tbody>
                    <tbody>
                      <tr>
                        <td rowspan="5" align="center">003 Azis Kalasan</td>
                        <td>Jumlah Plasma</td>
                        <td><?php echo $data_populasi_003->JumPlas; ?></td>
                        <td><?php echo $data_populasi_last_003->JumPlas; ?></td>
                        <td>
                          <?php if ($data_populasi_003->JumPlas >= $data_populasi_last_003->JumPlas): ?>
                            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>
                              <?php echo ceil((($data_populasi_003->JumPlas - $data_populasi_last_003->JumPlas)/$data_populasi_last_003->JumPlas*100))."%"; ?> 
                              </i> Dari yang lalu</span>
                          <?php else: ?>
                            <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo ceil((($data_populasi_003->JumPlas - $data_populasi_last_003->JumPlas)/$data_populasi_last_003->JumPlas*100))."%"; ?> </i> Dari yang lalu</span>
                          <?php endif ?>
                        </td>
                      </tr>
                      <tr>
                        <td>Jumlah Populasi</td>
                        <td><?php echo number_format($data_populasi_003->Populasi,0,'','.') ?></td>
                        <td><?php echo number_format($data_populasi_last_003->Populasi,0,'','.') ?></td>
                        <td>
                          <?php if ($data_populasi_003->Populasi >= $data_populasi_last_003->Populasi): ?>
                            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>
                              <?php echo ceil((($data_populasi_003->Populasi - $data_populasi_last_003->Populasi)/$data_populasi_last_003->Populasi*100))."%"; ?> 
                              </i> Dari yang lalu</span>
                          <?php else: ?>
                            <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo ceil((($data_populasi_003->Populasi - $data_populasi_last_003->Populasi)/$data_populasi_last_003->Populasi*100))."%"; ?> </i> Dari yang lalu</span>
                          <?php endif ?>
                        </td>
                      </tr>
                      <tr>
                        <td>Jumlah Surplus</td>
                        <td><?php echo number_format($data_populasi_003->Surplus,0,'','.'); ?></td>
                        <td><?php echo number_format($data_populasi_last_003->Surplus,0,'','.'); ?></td>
                        <td>
                          <?php if ($data_populasi_003->Surplus >= $data_populasi_last_003->Surplus): ?>
                            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>
                              <?php echo ceil((($data_populasi_003->Surplus - $data_populasi_last_003->Surplus)/$data_populasi_last_003->Surplus*100))."%"; ?> 
                              </i> Dari yang lalu</span>
                          <?php else: ?>
                            <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo ceil((($data_populasi_003->Surplus - $data_populasi_last_003->Surplus)/$data_populasi_last_003->Surplus*100))."%"; ?> </i> Dari yang lalu</span>
                          <?php endif ?>
                        </td>
                      </tr>
                      <tr>
                        <td>Jumlah Tabungan</td>
                        <td><?php echo number_format($data_populasi_003->Tabungan,0,'','.'); ?></td>
                        <td><?php echo number_format($data_populasi_last_003->Tabungan,0,'','.'); ?></td>
                        <td>
                          <?php if ($data_populasi_003->Tabungan >= $data_populasi_last_003->Tabungan): ?>
                            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>
                              <?php echo ceil((($data_populasi_003->Tabungan - $data_populasi_last_003->Tabungan)/$data_populasi_last_003->Tabungan*100))."%"; ?> 
                              </i> Dari yang lalu</span>
                          <?php else: ?>
                            <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo ceil((($data_populasi_003->Tabungan - $data_populasi_last_003->Tabungan)/$data_populasi_last_003->Tabungan*100))."%"; ?> </i> Dari yang lalu</span>
                          <?php endif ?>
                        </td>
                      </tr>
                      <tr>
                        <td>Rata - rata surplus</td>
                        <td><?php echo number_format($data_populasi_003->RataSurplus,0,'','.') ?></td>
                        <td><?php echo number_format($data_populasi_last_003->RataSurplus,0,'','.')  ?></td>
                        <td>
                          <?php if ($data_populasi_003->RataSurplus >= $data_populasi_last_003->RataSurplus): ?>
                            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>
                              <?php echo ceil((($data_populasi_003->RataSurplus - $data_populasi_last_003->RataSurplus)/$data_populasi_last_003->RataSurplus*100))."%"; ?> 
                              </i> Dari yang lalu</span>
                          <?php else: ?>
                            <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo ceil((($data_populasi_003->RataSurplus - $data_populasi_last_003->RataSurplus)/$data_populasi_last_003->RataSurplus*100))."%"; ?> </i> Dari yang lalu</span>
                          <?php endif ?>
                        </td>
                      </tr>
                    </tbody>
                    <tbody>
                      <tr>
                        <td rowspan="5" align="center">005 Rio Wonosobo</td>
                        <td>Jumlah Plasma</td>
                        <td><?php echo $data_populasi_005->JumPlas; ?></td>
                        <td><?php echo $data_populasi_last_005->JumPlas; ?></td>
                        <td>
                          <?php if ($data_populasi_005->JumPlas >= $data_populasi_last_005->JumPlas): ?>
                            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>
                              <?php echo ceil((($data_populasi_005->JumPlas - $data_populasi_last_005->JumPlas)/$data_populasi_last_005->JumPlas*100))."%"; ?> 
                              </i> Dari yang lalu</span>
                          <?php else: ?>
                            <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo ceil((($data_populasi_005->JumPlas - $data_populasi_last_005->JumPlas)/$data_populasi_last_005->JumPlas*100))."%"; ?> </i> Dari yang lalu</span>
                          <?php endif ?>
                        </td>
                      </tr>
                      <tr>
                        <td>Jumlah Populasi</td>
                        <td><?php echo number_format($data_populasi_005->Populasi,0,'','.'); ?></td>
                        <td><?php echo number_format($data_populasi_last_005->Populasi,0,'','.'); ?></td>
                        <td>
                          <?php if ($data_populasi_005->Populasi >= $data_populasi_last_005->Populasi): ?>
                            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>
                              <?php echo ceil((($data_populasi_005->Populasi - $data_populasi_last_005->Populasi)/$data_populasi_last_005->Populasi*100))."%"; ?> 
                              </i> Dari yang lalu</span>
                          <?php else: ?>
                            <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo ceil((($data_populasi_005->Populasi - $data_populasi_last_005->Populasi)/$data_populasi_last_005->Populasi*100))."%"; ?> </i> Dari yang lalu</span>
                          <?php endif ?>
                        </td>
                      </tr>
                      <tr>
                        <td>Jumlah Surplus</td>
                        <td><?php echo number_format($data_populasi_005->Surplus,0,'','.') ?></td>
                        <td><?php echo number_format($data_populasi_last_005->Surplus,0,'','.') ?></td>
                        <td>
                          <?php if ($data_populasi_005->Surplus >= $data_populasi_last_005->Surplus): ?>
                            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>
                              <?php echo ceil((($data_populasi_005->Surplus - $data_populasi_last_005->Surplus)/$data_populasi_last_005->Surplus*100))."%"; ?> 
                              </i> Dari yang lalu</span>
                          <?php else: ?>
                            <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo ceil((($data_populasi_005->Surplus - $data_populasi_last_005->Surplus)/$data_populasi_last_005->Surplus*100))."%"; ?> </i> Dari yang lalu</span>
                          <?php endif ?>
                        </td>
                      </tr>
                      <tr>
                        <td>Jumlah Tabungan</td>
                        <td><?php echo number_format($data_populasi_005->Tabungan, 0,'','.') ?></td>
                        <td><?php echo number_format($data_populasi_last_005->Tabungan, 0,'','.') ?></td>
                        <td>
                          <?php if ($data_populasi_005->Tabungan >= $data_populasi_last_005->Tabungan): ?>
                            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>
                              <?php echo ceil((($data_populasi_005->Tabungan - $data_populasi_last_005->Tabungan)/$data_populasi_last_005->Tabungan*100))."%"; ?> 
                              </i> Dari yang lalu</span>
                          <?php else: ?>
                            <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo ceil((($data_populasi_005->Tabungan - $data_populasi_last_005->Tabungan)/$data_populasi_last_005->Tabungan*100))."%"; ?> </i> Dari yang lalu</span>
                          <?php endif ?>
                        </td>
                      </tr>
                      <tr>
                        <td>Rata - rata surplus</td>
                        <td><?php echo number_format($data_populasi_005->RataSurplus, 0,'','.') ?></td>
                        <td><?php echo number_format($data_populasi_last_005->RataSurplus, 0,'','.') ?></td>
                        <td>
                          <?php if ($data_populasi_005->RataSurplus >= $data_populasi_last_005->RataSurplus): ?>
                            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>
                              <?php echo ceil((($data_populasi_005->RataSurplus - $data_populasi_last_005->RataSurplus)/$data_populasi_last_005->RataSurplus*100))."%"; ?> 
                              </i> Dari yang lalu</span>
                          <?php else: ?>
                            <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo ceil((($data_populasi_005->RataSurplus - $data_populasi_last_005->RataSurplus)/$data_populasi_last_005->RataSurplus*100))."%"; ?> </i> Dari yang lalu</span>
                          <?php endif ?>
                        </td>
                      </tr>
                    </tbody>
                    <tbody>
                      <tr>
                        <td rowspan="5" align="center">008 Sigit Sumpiuh</td>
                        <td>Jumlah Plasma</td>
                        <td><?php echo $data_populasi_008->JumPlas; ?></td>
                        <td><?php echo $data_populasi_last_008->JumPlas; ?></td>
                        <td>
                          <?php if ($data_populasi_008->JumPlas >= $data_populasi_last_008->JumPlas): ?>
                            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>
                              <?php echo ceil((($data_populasi_008->JumPlas - $data_populasi_last_008->JumPlas)/$data_populasi_last_008->JumPlas*100))."%"; ?> 
                              </i> Dari yang lalu</span>
                          <?php else: ?>
                            <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo ceil((($data_populasi_008->JumPlas - $data_populasi_last_008->JumPlas)/$data_populasi_last_008->JumPlas*100))."%"; ?> </i> Dari yang lalu</span>
                          <?php endif ?>
                        </td>
                      </tr>
                      <tr>
                        <td>Jumlah Populasi</td>
                        <td><?php echo number_format($data_populasi_008->Populasi, 0,'','.'); ?></td>
                        <td><?php echo number_format($data_populasi_last_008->Populasi, 0,'','.'); ?></td>
                        <td>
                          <?php if ($data_populasi_008->Populasi >= $data_populasi_last_008->Populasi): ?>
                            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>
                              <?php echo ceil((($data_populasi_008->Populasi - $data_populasi_last_008->Populasi)/$data_populasi_last_008->Populasi*100))."%"; ?> 
                              </i> Dari yang lalu</span>
                          <?php else: ?>
                            <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo ceil((($data_populasi_008->Populasi - $data_populasi_last_008->Populasi)/$data_populasi_last_008->Populasi*100))."%"; ?> </i> Dari yang lalu</span>
                          <?php endif ?>
                        </td>
                      </tr>
                      <tr>
                        <td>Jumlah Surplus</td>
                        <td><?php echo number_format($data_populasi_008->Surplus, 0,'','.'); ?></td>
                        <td><?php echo number_format($data_populasi_last_008->Surplus, 0,'','.'); ?></td>
                        <td>
                          <?php if ($data_populasi_008->Surplus >= $data_populasi_last_008->Surplus): ?>
                            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>
                              <?php echo ceil((($data_populasi_008->Surplus - $data_populasi_last_008->Surplus)/$data_populasi_last_008->Surplus*100))."%"; ?> 
                              </i> Dari yang lalu</span>
                          <?php else: ?>
                            <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo ceil((($data_populasi_008->Surplus - $data_populasi_last_008->Surplus)/$data_populasi_last_008->Surplus*100))."%"; ?> </i> Dari yang lalu</span>
                          <?php endif ?>
                        </td>
                      </tr>
                      <tr>
                        <td>Jumlah Tabungan</td>
                        <td><?php echo number_format($data_populasi_008->Tabungan, 0,'','.') ?></td>
                        <td><?php echo number_format($data_populasi_last_008->Tabungan, 0,'','.') ?></td>
                        <td>
                          <?php if ($data_populasi_008->Tabungan >= $data_populasi_last_008->Tabungan): ?>
                            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>
                              <?php echo ceil((($data_populasi_008->Tabungan - $data_populasi_last_008->Tabungan)/$data_populasi_last_008->Tabungan*100))."%"; ?> 
                              </i> Dari yang lalu</span>
                          <?php else: ?>
                            <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo ceil((($data_populasi_008->Tabungan - $data_populasi_last_008->Tabungan)/$data_populasi_last_008->Tabungan*100))."%"; ?> </i> Dari yang lalu</span>
                          <?php endif ?>
                        </td>
                      </tr>
                      <tr>
                        <td>Rata - rata surplus</td>
                        <td><?php echo number_format($data_populasi_008->RataSurplus,0,'','.') ?></td>
                        <td><?php echo number_format($data_populasi_last_008->RataSurplus,0,'','.') ?></td>
                        <td>
                          <?php if ($data_populasi_008->RataSurplus >= $data_populasi_last_008->RataSurplus): ?>
                            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>
                              <?php echo ceil((($data_populasi_008->RataSurplus - $data_populasi_last_008->RataSurplus)/$data_populasi_last_008->RataSurplus*100))."%"; ?> 
                              </i> Dari yang lalu</span>
                          <?php else: ?>
                            <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo ceil((($data_populasi_008->RataSurplus - $data_populasi_last_008->RataSurplus)/$data_populasi_last_008->RataSurplus*100))."%"; ?> </i> Dari yang lalu</span>
                          <?php endif ?>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>

          <br />
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php $this->load->view('adminpages/footer'); ?>
        <!-- /footer content -->
      </div>
    </div>

<?php require_once(APPPATH .'views/include/admin/inc_script.php'); ?>
	<script type="text/javascript">
      jQuery(document).ready(function($) {
        var jum_plasma001 = $('#jum_plasma001').attr("data-id");
        var populasi001 = $('#populasi001').attr("data-id");
        var surplus001 = $('#surplus001').attr("data-id");
        var tabungan001 = $('#tabungan001').attr("data-id");
        var ratarata001 = $('#rata-rata001').attr("data-id");

        var jum_plasma003 = $('#jum_plasma003').attr("data-id");
        var populasi003 = $('#populasi003').attr("data-id");
        var surplus003 = $('#surplus003').attr("data-id");
        var tabungan003 = $('#tabungan003').attr("data-id");
        var ratarata003 = $('#rata-rata003').attr("data-id");

        var jum_plasma008 = $('#jum_plasma008').attr("data-id");
        var populasi008 = $('#populasi008').attr("data-id");
        var surplus008 = $('#surplus008').attr("data-id");
        var tabungan008 = $('#tabungan008').attr("data-id");
        var ratarata008 = $('#rata-rata008').attr("data-id");

        var jum_plasma005 = $('#jum_plasma005').attr("data-id");
        var populasi005 = $('#populasi005').attr("data-id");
        var surplus005 = $('#surplus005').attr("data-id");
        var tabungan005 = $('#tabungan005').attr("data-id");
        var ratarata005 = $('#rata-rata005').attr("data-id");

        var options = {
          useEasing: true, 
          useGrouping: true, 
          separator: '.', 
          decimal: '.', 
        };
        
        var jum_plasma001 = new CountUp(document.getElementById("jum_plasma001"), 0, jum_plasma001, 0, 2.9, options);
        var populasi001 = new CountUp(document.getElementById("populasi001"), 0, populasi001, 0, 2.9, options);
        var surplus001 = new CountUp(document.getElementById("surplus001"), 0, surplus001, 0, 2.9, options);
        var tabungan001 = new CountUp(document.getElementById("tabungan001"), 0, tabungan001, 0, 2.9, options);
        var ratarata001 = new CountUp(document.getElementById("rata-rata001"), 0, ratarata001, 0, 2.9, options);

        var jum_plasma003 = new CountUp(document.getElementById("jum_plasma003"), 0, jum_plasma003, 0, 2.9, options);
        var populasi003 = new CountUp(document.getElementById("populasi003"), 0, populasi003, 0, 2.9, options);
        var surplus003 = new CountUp(document.getElementById("surplus003"), 0, surplus003, 0, 2.9, options);
        var tabungan003 = new CountUp(document.getElementById("tabungan003"), 0, tabungan003, 0, 2.9, options);
        var ratarata003 = new CountUp(document.getElementById("rata-rata003"), 0, ratarata003, 0, 2.9, options);

        var jum_plasma008 = new CountUp(document.getElementById("jum_plasma008"), 0, jum_plasma008, 0, 2.9, options);
        var populasi008 = new CountUp(document.getElementById("populasi008"), 0, populasi008, 0, 2.9, options);
        var surplus008 = new CountUp(document.getElementById("surplus008"), 0, surplus008, 0, 2.9, options);
        var tabungan008 = new CountUp(document.getElementById("tabungan008"), 0, tabungan008, 0, 2.9, options);
        var ratarata008 = new CountUp(document.getElementById("rata-rata008"), 0, ratarata008, 0, 2.9, options);

        var jum_plasma005 = new CountUp(document.getElementById("jum_plasma005"), 0, jum_plasma005, 0, 2.9, options);
        var populasi005 = new CountUp(document.getElementById("populasi005"), 0, populasi005, 0, 2.9, options);
        var surplus005 = new CountUp(document.getElementById("surplus005"), 0, surplus005, 0, 2.9, options);
        var tabungan005 = new CountUp(document.getElementById("tabungan005"), 0, tabungan005, 0, 2.9, options);
        var ratarata005 = new CountUp(document.getElementById("rata-rata005"), 0, ratarata005, 0, 2.9, options);

        jum_plasma001.start();
        populasi001.start();
        surplus001.start();
        tabungan001.start();
        ratarata001.start();

        jum_plasma003.start();
        populasi003.start();
        surplus003.start();
        tabungan003.start();
        ratarata003.start();

        jum_plasma008.start();
        populasi008.start();
        surplus008.start();
        tabungan008.start();
        ratarata008.start();

        jum_plasma005.start();
        populasi005.start();
        surplus005.start();
        tabungan005.start();
        ratarata005.start();
        
      });
      
    </script>

    
  </body>
</html>
