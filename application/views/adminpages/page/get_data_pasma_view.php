<style type="text/css">
  .tile-stats .count {
        font-size: 25px;
        font-weight: 700;
        line-height: 2.65857;
    }

    .tile-stats h3 {
        color: #BAB8B8;
        font-size: 16px;
    }
</style>

<!-- top tiles -->
<div class="row tile_count">
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-calendar"></i> Periode</span>
    <div class="count"><?php echo $data_periode; ?></div>
    <span class="count_bottom"><i class="green"><i class="fa fa-calendar"></i> <?php echo $data_periode+1; ?> </i> Periode Selanjutnya</span>
  </div>
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-calculator"></i> Jumlah Plasma</span>
    <div class="count" id="jum_plasma" data-id="<?php echo $data_populasi->JumPlas; ?>"></div>
    <?php if ($data_populasi->JumPlas >= $data_populasi_last->JumPlas): ?>
    <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>
      <?php echo ceil((($data_populasi->JumPlas - $data_populasi_last->JumPlas)/$data_populasi_last->JumPlas*100))."%"; ?>
    </i> Dari yang lalu</span>
    <?php else: ?>
    <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo ceil((($data_populasi->JumPlas - $data_populasi_last->JumPlas)/$data_populasi_last->JumPlas*100))."%"; ?> </i> Dari yang lalu</span>
    <?php endif ?>
  </div>
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-users"></i> Total Populasi</span>
    <div class="count green" id="populasi" data-id="<?php echo $data_populasi->Populasi ?>"></div>
    <?php if ($data_populasi->Populasi >= $data_populasi_last->Populasi): ?>
    <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>
      <?php echo ceil((($data_populasi->Populasi - $data_populasi_last->Populasi)/$data_populasi_last->Populasi*100))."%"; ?>
    </i> Dari yang lalu</span>
    <?php else: ?>
    <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo ceil((($data_populasi->Populasi - $data_populasi_last->Populasi)/$data_populasi_last->Populasi*100))."%"; ?> </i> Dari yang lalu</span>
    <?php endif ?>
  </div>
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-clipboard"></i> Total Surplus</span>
    <div class="count" id="surplus" data-id="<?php echo$data_populasi->Surplus ?>"></div>
    <?php if ($data_populasi->Surplus >= $data_populasi_last->Surplus): ?>
    <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>
      <?php echo ceil((($data_populasi->Surplus - $data_populasi_last->Surplus)/$data_populasi_last->Surplus*100))."%"; ?>
    </i> Dari yang lalu</span>
    <?php else: ?>
    <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo ceil((($data_populasi->Surplus - $data_populasi_last->Surplus)/$data_populasi_last->Surplus*100))."%"; ?> </i> Dari yang lalu</span>
    <?php endif ?>
  </div>
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-line-chart"></i> Total Tabungan</span>
    <div class="count" id="tabungan" data-id="<?php echo $data_populasi->Tabungan ?>"></div>
    <?php if ($data_populasi->Tabungan >= $data_populasi_last->Tabungan): ?>
    <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>
      <?php echo ceil((($data_populasi->Tabungan - $data_populasi_last->Tabungan)/$data_populasi_last->Tabungan*100))."%"; ?>
    </i> Dari yang lalu</span>
    <?php else: ?>
    <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo ceil((($data_populasi->Tabungan - $data_populasi_last->Tabungan)/$data_populasi_last->Tabungan*100))."%"; ?> </i> Dari yang lalu</span>
    <?php endif ?>
  </div>
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-bar-chart-o"></i> Rata Rata Surplus</span>
    <div class="count" id="rata-rata" data-id="<?php echo $data_populasi->RataSurplus; ?>"></div>
    <?php if ($data_populasi->RataSurplus >= $data_populasi_last->RataSurplus): ?>
    <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>
      <?php echo ceil((($data_populasi->RataSurplus - $data_populasi_last->RataSurplus)/$data_populasi_last->RataSurplus*100))."%"; ?>
    </i> Dari yang lalu</span>
    <?php else: ?>
    <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo ceil((($data_populasi->RataSurplus - $data_populasi_last->RataSurplus)/$data_populasi_last->RataSurplus*100))."%"; ?> </i> Dari yang lalu</span>
    <?php endif ?>
  </div>
</div>
<!-- /top tiles -->

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <h3>Periode Lalu <b><?php echo $data_periode_last ?></b></h3>
    <div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">
      <div class="tile-stats">
        <div class="icon"><i class="fa fa-calendar"></i> </i>
        </div>
        <div class="count"><?php echo $data_periode_last ?></div>
        <h3>Periode</h3>
        <p></p>
      </div>
    </div>
    <div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">
      <div class="tile-stats">
        <div class="icon"><i class="fa fa-calculator"></i></i>
        </div>
        <div class="count"><?php echo number_format($data_populasi_last->JumPlas,0,'','.'); ?></div>
        <h3>Jumlah Plasma</h3>
        <p></p>
      </div>
    </div>
    <div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">
      <div class="tile-stats">
        <div class="icon"><i class="fa fa-users"></i></i>
        </div>
        <div class="count"><?php echo number_format($data_populasi_last->Populasi,0,'','.'); ?></div>
        <h3>Total Populasi</h3>
        <p></p>
      </div>
    </div>
    <div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">
      <div class="tile-stats">
        <div class="icon"><i class="fa fa-clipboard"></i></i>
        </div>
        <div class="count"><?php echo number_format($data_populasi_last->Surplus,0,'','.'); ?></div>
        <h3>Total Surplus</h3>
        <p></p>
      </div>
    </div>
    <div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">
      <div class="tile-stats">
        <div class="icon"><i class="fa fa-line-chart"></i></i>
        </div>
        <div class="count"><?php echo number_format($data_populasi_last->Tabungan,0,'','.'); ?></div>
        <h3>Total Tabungan</h3>
        <p></p>
      </div>
    </div>
    <div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">
      <div class="tile-stats">
        <div class="icon"><i class="fa fa-bar-chart-o"></i></i>
        </div>
        <div class="count"><?php echo number_format($data_populasi_last->RataSurplus,0,'','.'); ?></div>
        <h3>Rata Rata Surplus</h3>
        <p></p>
      </div>
    </div>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h3>Data Populasi Sekarang</b></h3>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="table-responsive">
          <table id="datatable-buttons" class="table table-striped table-bordered">
            <thead>
              <tr>
                <!-- <th>#</th> -->
                <!-- <th>Kode Plasma</th> -->
                <th>Nama Plasma</th>
                <th>Populasi</th>
                <th>Surplus</th>
                <th>Tabungan</th>
              </tr>
            </thead>
            <tbody>
              <?php $i = 1; foreach ($data_plasma as $dt_plasma): ?>
                <tr>
                  <!-- <td><?php echo $i++; ?></td> -->
                  <!-- <td><?php echo $dt_plasma->Kode_Plas; ?></td> -->
                  <td><?php echo $dt_plasma->Namaplas; ?></td>
                  <td><?php echo number_format($dt_plasma->Populasi,0,'','.'); ?></td>
                  <td><?php echo number_format($dt_plasma->Surplus,0,'','.'); ?></td>
                  <td><?php echo number_format($dt_plasma->Tabungan,0,'','.'); ?></td>
                </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<?php require_once(APPPATH .'views/include/admin/inc_script.php'); ?>
<script type="text/javascript">
  jQuery(document).ready(function($) {
    var jum_plasma = $('#jum_plasma').attr("data-id");
    var populasi = $('#populasi').attr("data-id");
    var surplus = $('#surplus').attr("data-id");
    var tabungan = $('#tabungan').attr("data-id");
    var ratarata = $('#rata-rata').attr("data-id");

    var options = {
      useEasing: true, 
      useGrouping: true, 
      separator: '.', 
      decimal: '.', 
    };
    var jum_plasma = new CountUp(document.getElementById("jum_plasma"), 0, jum_plasma, 0, 2.5, options);
    var populasi = new CountUp(document.getElementById("populasi"), 0, populasi, 0, 2.5, options);
    var surplus = new CountUp(document.getElementById("surplus"), 0, surplus, 0, 2.5, options);
    var tabungan = new CountUp(document.getElementById("tabungan"), 0, tabungan, 0, 2.5, options);
    var ratarata = new CountUp(document.getElementById("rata-rata"), 0, ratarata, 0, 2.5, options);
    jum_plasma.start();
    populasi.start();
    surplus.start();
    tabungan.start();
    ratarata.start();
    
  });
  
</script>