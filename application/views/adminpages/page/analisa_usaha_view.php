<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/inc_style.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php $this->load->view('adminpages/main_menu'); ?>

        <!-- top navigation -->
        <?php $this->load->view('adminpages/nav_bar'); ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Analisa Usaha </h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                      
                    </p>
                    <table class="table">
                      <thead>
                        <tr>
                          <th>Umur</th>
                          <th>Populasi</th>
                          <th>Intake</th>
                          <th>Pakan</th>
                          <th>Harga Zak</th>
                          <th>Harga Pakan</th>
                          <th>Prod (%)</th>
                          <th>Prod (Butir)</th>
                          <th>Prod Harga</th>
                          <th>Harga Telur</th>
                          <th>Laba</th>
                          <th>Pendapatan</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $jumlah = 0; ?>
                        <?php foreach ($data_analisa as $dt_analisa): ?>
                        <tr>
                          <td><?php echo $dt_analisa->umur ?></td>
                          <td><?php echo $dt_analisa->populasi ?></td>
                          <td><?php echo $dt_analisa->intake ?></td>
                          <td><?php echo $dt_analisa->pakan ?></td>
                          <td><?php echo number_format($dt_analisa->harga_per_zak,0,'','.') ?></td>
                          <td><?php echo number_format($dt_analisa->harga_pakan,0,'','.') ?></td>
                          <td><?php echo $dt_analisa->prod_persen ?></td>
                          <td><?php echo number_format($dt_analisa->prod_butir,0,'','.') ?></td>
                          <td><?php echo $dt_analisa->harga_per_butir ?></td>
                          <td><?php echo number_format($dt_analisa->harga_telur,0,'','.') ?></td>
                          <td><?php echo number_format($dt_analisa->laba,0,'','.') ?></td>
                          <td><?php echo number_format(ceil($dt_analisa->pendapatan), 0,'','.') ?></td>
                        </tr>
                        <?php $jumlah = $jumlah + $dt_analisa->laba ?>
                        <?php endforeach ?>
                        <tr>
                          <td colspan="10"></td>
                          <td style="background: red; color: #fff;"><?php echo number_format(ceil($jumlah),0,'','.')  ?></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php $this->load->view('adminpages/footer'); ?>
        <!-- /footer content -->
      </div>
    </div>

<?php require_once(APPPATH .'views/include/admin/inc_script.php'); ?>
  
  </body>
</html>
