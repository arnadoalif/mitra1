<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/inc_style.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php $this->load->view('adminpages/main_menu'); ?>

        <!-- top navigation -->
        <?php $this->load->view('adminpages/nav_bar'); ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="row">

              <div class="col-md-12 col-xs-12">
                <div class="table-responsive">
                  <?php $this->load->model('Command_model'); $m_command = new Command_model(); ?>
                  <table id="datatable" class="table table-hover">
                    <thead>
                      <tr>
                        <th>Topik</th>
                        <th>Command</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($data_konten as $dt_konten): ?>
                      <tr>
                        <td><?php echo $dt_konten->title_konten; ?></td>
                        <td><?php echo $m_command->count_data_command_by_kode_kontent('Tbl_Comment', $dt_konten->kode_konten) ?></td>
                        <td>
                          <a class="btn btn-sm btn-info" target="_blank" href="<?php echo base_url('konten/'.$dt_konten->slug_url) ?>" role="button"><i class="fa fa-eye"></i> View</a>
                          <a class="btn btn-sm btn-default" role="button" data-toggle="modal" href="<?php echo base_url('super/edit/'.$dt_konten->kode_konten)?>"><i class="fa fa-pencil"></i> Edit</a>
                          <a class="btn btn-sm btn-danger" href="<?php echo base_url('super/delete/'.$dt_konten->kode_konten); ?>" role="button"><i class="fa fa-trash"></i> Delete</a>
                        </td>
                      </tr>
                      <?php endforeach ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          <br />

        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php $this->load->view('adminpages/footer'); ?>
        <!-- /footer content -->
      </div>
    </div>

<?php require_once(APPPATH .'views/include/admin/inc_script.php'); ?>
	<script type="text/javascript">
  jQuery(document).ready(function($) {
     CKEDITOR.replace( 'ckeditor2' );
  });
</script>
  </body>
</html>
