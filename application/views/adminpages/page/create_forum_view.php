<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/inc_style.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php $this->load->view('adminpages/main_menu'); ?>

        <!-- top navigation -->
        <?php $this->load->view('adminpages/nav_bar'); ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="row">
              <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Buat Tulisan</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <?php if (isset($_SESSION['message_data'])): ?>
                      <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <?php echo $_SESSION['message_data'] ?>
                      </div>
                      <?php endif ?>

                      <?php if (isset($_SESSION['error_data'])): ?>
                      <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <?php echo $_SESSION['error_data'] ?>
                      </div>
                    <?php endif ?>

                    <br />
                    <form class="form-horizontal form-label-left" method="POST" action="<?php echo base_url('super/action_create_forum'); ?>" enctype="multipart/form-data">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Judul</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" name="judul_topik" class="form-control" placeholder="Judul Topik">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Kategori</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <select name="kategori_topik" id="inputKategori_topik" class="form-control" required="required">
                            <option value="" selected>-- Kategori Topik --</option>
                            <option value="persiapan">Persiapan</option>
                            <option value="starter_puyuh">Starter Puyuh</option>
                            <option value="doq">DOQ</option>
                            <option value="pemeliharaan">Pemeliharaan</option>
                            <option value="panen">Panen</option>
                            <option value="penyakit">Penyakit</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Isi Konten</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <textarea name="isi_topik" id="ckeditor1" class="form-control" rows="8" style="width: 100%; height: 100%;" required="required"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Cover</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="file" name="thubnail" class="form-control" value="" required="required">
                        </div>
                      </div>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="submit" class="btn btn-success">Submit</button>
                          <button type="reset" class="btn btn-warning">Reset</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>

              <div class="col-md-12 col-xs-12">
                <div class="table-responsive">
                  <table id="datatable" class="table table-hover">
                    <thead>
                      <tr>
                        <th>Forum</th>
                        <th>Topik</th>
                        <th>View</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($data_kategori as $dt_kategori): ?>
                        
                      <tr>
                        <td><a href="<?php echo base_url('kategori/'.$dt_kategori->kategori); ?>" title="<?php echo $dt_kategori->kategori ?>"><?php echo str_replace('_', ' ', ucfirst($dt_kategori->kategori));  ?></a></td>
                        <td><?php echo $dt_kategori->total_data ?></td>
                        <td><a class="btn btn-sm btn-warning" href="<?php echo base_url('super/open/'.$dt_kategori->kategori); ?>" role="button"><i class="fa fa-eye"></i> Open Konten</a></td>
                      </tr>

                      <?php endforeach ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          <br />

        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php $this->load->view('adminpages/footer'); ?>
        <!-- /footer content -->
      </div>
    </div>

<?php require_once(APPPATH .'views/include/admin/inc_script.php'); ?>
	<script type="text/javascript">
  jQuery(document).ready(function($) {
     CKEDITOR.replace( 'ckeditor1' );
  });
</script>
  </body>
</html>
