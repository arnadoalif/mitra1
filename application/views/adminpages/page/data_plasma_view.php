<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/inc_style.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php $this->load->view('adminpages/main_menu'); ?>

        <!-- top navigation -->
        <?php $this->load->view('adminpages/nav_bar'); ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Data Plasma </h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Periode <b><?php echo $data_periode ?></b></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                      
                    </p>
                    <table id="datatable-fixed-header" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>No Urut</th>
                          <th>Kode Plasma</th>
                          <th>Nama Plasma</th>
                          <th>Populasi</th>
                          <th>Surplus</th>
                          <th>Tabungan</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $i = 1; foreach ($data_plasma as $dt_plasma): ?>
                        <tr>
                          <td><?php echo $i++; ?></td>
                          <td><?php echo $dt_plasma->Kode_Plas; ?></td>
                          <td><?php echo $dt_plasma->Namaplas; ?></td>
                          <td><?php echo number_format($dt_plasma->Populasi,0,'','.');?></td>
                          <td><?php echo number_format($dt_plasma->Surplus,0,'','.');?></td>
                          <td><?php echo number_format($dt_plasma->Tabungan,0,'','.');?></td>
                        </tr>
                        <?php endforeach ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php $this->load->view('adminpages/footer'); ?>
        <!-- /footer content -->
      </div>
    </div>

<?php require_once(APPPATH .'views/include/admin/inc_script.php'); ?>
	
  </body>
</html>
