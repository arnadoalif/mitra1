<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/inc_style.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php $this->load->view('adminpages/main_menu'); ?>

        <!-- top navigation -->
        <?php $this->load->view('adminpages/nav_bar'); ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph">

                <div class="row x_title">
                  <div class="col-md-6">
                    <h3>Periode <?php echo $data_periode; ?></h3>
                  </div>
                  <div class="col-md-6 col-sm-12">
                    <select name="" id="option" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc" class="form-control" required="required">
                      <option value="" selected>- Pilih Koordinator -</option>
                      <option value="001">001 | Nanang Brosot</option>
                      <option value="003">003 | Azis Kalasan</option>
                      <option value="005">005 | Rio Wonosobo</option>
                      <option value="008">008 | Sigit Sumpiuh</option>
                    </select>
                  </div>
                </div>
                
                <div id="view_data"></div>
                

                <div class="clearfix"></div>
              </div>
            </div>

          </div>
          <br />

        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php $this->load->view('adminpages/footer'); ?>
        <!-- /footer content -->
      </div>
    </div>

<?php require_once(APPPATH .'views/include/admin/inc_script.php'); ?>
	<script type="text/javascript">
   jQuery(document).ready(function($) {
      $('#option').change(function(event) {
        
        var id = $(this).val();
        get_data_plasma_id(id);
        console.log(id);

      });

      function get_data_plasma_id(kode) {
        console.log(kode);

        $.ajax({
          url: '<?php echo base_url()?>super/id_plasma',
          type: 'POST',
          dataType: 'html',
          data: {kode: kode},
        })
        .done(function(e) {
          $('#view_data').html(e);
          console.log("success");
        })
        .fail(function(e) {
          console.log("error");
        })
        .always(function(e) {
          console.log("complete");
        });
        
      }
    }); 
  </script>
  </body>
</html>
