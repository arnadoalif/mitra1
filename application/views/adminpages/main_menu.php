<div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span><?php echo ucfirst($this->session->userdata('user_name')) ?></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url('asset_admin/images/avatar/'.$this->session->userdata('avatar')); ?> " alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php echo ucfirst($this->session->userdata('nama')); ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <?php if ( $this->session->userdata('Session_log') != 200): ?>
                    <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-home"></i> Home </a></li>
                    <li><a href="<?php echo base_url('admin/data_plasma'); ?>"><i class="fa fa-table"></i> Data Plasma </a></li>
                  <?php else: ?>
                    <li><a href="<?php echo base_url('super'); ?>"><i class="fa fa-home"></i> Home Super </a></li>
                    <li><a href="<?php echo base_url('super/data_plasma'); ?>"><i class="fa fa-table"></i> Data Plasma Super</a></li>
                    <!-- <li><a href="<?php echo base_url('super/analisa'); ?>"><i class="fa fa-table"></i> Data Analisa</a></li> -->
                    <li><a href="<?php echo base_url('super/create_forum'); ?>"><i class="fa fa-forumbee"></i> Forum </a></li>
                  <?php endif ?>
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
             <!--  <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a> -->
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo base_url('logout'); ?>">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

