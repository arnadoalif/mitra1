<div class="banner">
		<div class="container top-banner">
			<div class="w3_agile_banner_top">
				<div class="agile_phone_mail">
					<ul>
						<li><i class="fa fa-map-marker" aria-hidden="true"></i>Pulo, Brosot, Galur, Kulonprogo</li>
						<li><i class="fa fa-phone" aria-hidden="true"></i>081328818656</li>
					</ul>
				</div>
			</div>
			<div class="agileits_w3layouts_banner_nav">
				<nav class="navbar navbar-default">
					<div class="navbar-header navbar-left">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<h1>
							<a class="navbar-brand" href="<?php echo base_url(); ?>">
								<img src="<?php echo base_url('asset_front/images/logo_guyup_2.png'); ?>" class="img-responsive hidden-xs hidden-sm" alt="Image">
								<img src="<?php echo base_url('asset_front/images/logo_guyup_3.png'); ?>" class="img-responsive hidden-lg hidden-md" alt="Image">
							</a>
						</h1>
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
						<nav class="cl-effect-13" id="cl-effect-13">
							<ul class="nav navbar-nav">
								<li class="active"><a href="<?php echo base_url('home'); ?>">Beranda</a></li>
								<li><a href="<?php echo base_url('profil'); ?>">Profil</a></li>
								<li><a href="<?php echo base_url('analisa_usaha'); ?>">Analisa Usaha</a></li>
								<li><a href="<?php echo base_url('forum'); ?>">Forum</a></li>
								<!-- <li><a href="">Blog</a></li> -->
								<li><a href="<?php echo base_url('daftar_guyu'); ?>">Daftar Guyubingmanah</a></li>
							</ul>
						</nav>
					</div>
				</nav>
			</div>
		</div>
	</div>