<!DOCTYPE html>
<html lang="en">
<head>
<title>Guyubingmanah</title>
<!-- custom-theme -->
<?php require_once(APPPATH .'views/include/front/front_inc_style.php'); ?>
</head>
<body>

<!-- banner -->

	<div class="banner">
		<div class="top-banner">
			<div class="w3_agile_banner_top hidden-xs">
				<div class="agile_phone_mail">
					<ul>
						<li><i class="fa fa-phone" aria-hidden="true"></i>Pulo, Brosot, Galur, Kulonprogo</li>
						<li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:guyupbingmanah@gmail.com">guyubingmanah@gmail.com</a></li>
					</ul>
				</div>
			</div>
			<div class="agileits_w3layouts_banner_nav">
				<nav class="navbar navbar-default">
					<div class="navbar-header navbar-left">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<h1><a class="navbar-brand" href=""><img src="<?php echo base_url('asset_front/images/new/logo_guyup_2.png'); ?> " class="" alt=""></a></h1>
					</div>
					<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
						<nav class="cl-effect-13" id="cl-effect-13">
						<ul class="nav navbar-nav">
							<li class="active"><a href="">Beranda</a></li>
							<li><a href="">Profil</a></li>
							<li><a href="">Kontak</a></li>
							<li><a target="_blank" href="<?php echo base_url('login'); ?>">Login</a></li>
						</ul>
						
					</nav>

					</div>
				</nav>
			</div>
		</div>
		<div class="container-fluid pd-slide" >
			<div class="wthree_banner_info">
				<section class="slider">
					<div class="flexslider">

						<ul class="slides">
							<li>
								<div class="slider-left col-md-6">
									<h3>Jadilah Jutawan Bersama Kami</h3>
									<p>Raih Sukses Berternak Puyuh Telur Bersama Kmai, <span class="text-blod">Guyubingmanah.</span> Peluang Usaha Dengan <span class="text-blod">Modal Terjangkau, Hasil Pasti</span> dan <span class="text-blod">Tidak Ribet</span></p>
									<div class="agileits_more">
										<ul>
											<li><a href="#" class=""><img src="<?php echo base_url('asset_front/images/new/btn_head_1.png'); ?>" class="img-responsive" alt="Image"></a></li>
											<li><a href="#" class=""><img src="<?php echo base_url('asset_front/images/new/btn_head_2.png'); ?>" class="img-responsive" alt="Image"></a></li>
										</ul>
									</div>
								</div>
								<div class="slider-right col-md-6">
									<img src="<?php echo base_url('asset_front/images/new/img_slide.png'); ?>" class="img-responsive img-slide-right" alt="Image">
								</div>
							</li>
							<li>
								<div class="slider-left col-md-6">
									<h3>Ternak Puyuh Tidak Memerlukan Lahan Luas</h3>
									<p>Anda bisa memanfaatkan halaman belakang rumah anda untuk dijadikan pendapatan tambahan untuk keluarga anda</p>
									<div class="agileits_more">
										<ul>
											<li><a href="#" class=""><img src="<?php echo base_url('asset_front/images/new/btn_head_1.png'); ?>" class="img-responsive" alt="Image"></a></li>
											<li><a href="#" class=""><img src="<?php echo base_url('asset_front/images/new/btn_head_2.png'); ?>" class="img-responsive" alt="Image"></a></li>
										</ul>
									</div>
								</div>
								<div class="slider-right col-md-6">
									<img src="<?php echo base_url('asset_front/images/new/img_slide_1.png'); ?>" class="img-responsive img-slide-right" alt="Image">
								</div>
								
							</li>
							<li>
								<div class="slider-left col-md-6">
									<h3>Usaha Sampingan Tanpa Mengganggu Aktivitas Utama</h3>
									<p>Berternak Puyuh Petelur Bersama <span class="text-blod">Guyubingmanah.</span> Tidak Mengganggu Aktivitas/pekerjaan Utama Anda, Ayo manfaatkan Waktu Luang Anda sekarang juga, <span class="text-blod">Dapatkan Ratusan Ribu Hingga Jutaan Rupiah tiap Minggunya</span></p>
									<div class="agileits_more">
										<ul>
											<li><a href="#" class=""><img src="<?php echo base_url('asset_front/images/new/btn_head_1.png'); ?>" class="img-responsive" alt="Image"></a></li>
											<li><a href="#" class=""><img src="<?php echo base_url('asset_front/images/new/btn_head_2.png'); ?>" class="img-responsive" alt="Image"></a></li>
										</ul>
									</div>
								</div>
								<div class="slider-right col-md-6">
									<img src="<?php echo base_url('asset_front/images/new/img_slide_2.png'); ?>" class="img-responsive img-slide-right" alt="Image">
								</div>
								
							</li>
						</ul>
					</div>
				</section>
					
			</div>
		</div>

		<div class="img-right-petani">
			<img src="<?php echo base_url('asset_front/images/new/body_right_1.png'); ?> " class="img-responsive" alt="Image">
		</div>
		<div class="profil">
			<div class="img-left-mascot">
				<img src="<?php echo base_url('asset_front/images/new/mascot_puyuh_1.png'); ?> " class="img-responsive" alt="Image">
			</div>
			<div class="text-profil">
				<div class="desc-profil">
					<h3>Sekilas Tentang Guyubingmanah</h3>
					<p>
						<strong>GUYUBING MANAH</strong> merupakan paguyuban dan kemitraan puyuh petelur terbesar di Jawa Tengah - D.I. Yogyakarta yang telah berpengalaman lebih dari 15 tahun membimbing peternak kecil di wilayah Jawa Tengah dan Yogyakarta menjadi peternak yang mandiri, tangguh dan sukses.
						<br><br>
						Kami telah mencetak ratusan peternak puyuh sukses, yang kami bimbing mulai dari nol hingga berhasil dari berbagai macam latar belakang profesi, pendidikan, usia yang berbeda.
						<br><br>
						Dengan bergabung dengan kemitraan kami, Anda akan kami bimbing mulai dari tahap awal hingga BERHASIL, seluruh bibit dan pakan kami sediakan dari pabrikan terpercaya demi hasil yang maksimal.
					</p>
				</div>
				<div class="desc-keuntungan">
					<h3>Mengapa Anda Harus Bergabung Dengan Guyubingmanah</h3>
					<p>
						<ul>
							<li>Keuntungan Puyuh Petelur Menjanjikan.</li>
							<li>Paguyuban Puyuh Petelur Terbesar Di Jogja - Jawa Tengah.</li>
							<li>Berpengalaman Lebih Dari 15 Tahun.</li>
							<li>Telah Mencetak Ratusan Peternak Puyuh Sukses.</li>
							<li>Bimbingan Aktif Kepada Anggota Kemitraan.</li>
						</ul>
					</p>
				</div>
				
			</div>
		</div>
		<div class="img-right-asset">
			<img src="<?php echo base_url('asset_front/images/new/body_right_2.png'); ?> " class="img-responsive" alt="Image">
		</div>
	</div>
	<section class="alasan-ternak">
		<div class="container-fluid pd-100">
			<div class="row">
				<h3 class="title-alasan">Alasan Kenapa Anda Harus Berternak Puyuh</h3>
				<div class="col-lg-4">
					
					<div class="col-lg-12">
						<div class="list-left">
							<div class="list-icon">
								<img src="<?php echo base_url('asset_front/images/new/icon_1.png'); ?>" class="img-responsive" alt="Image">
							</div>
							<div class="left-text">
								<h3>01</h3>
								<h4>Tabungan & Investasi</h4>
								<p>Puyuh mulai bertelur saat berumur 45 hari. masa produktif sampai 14bulan.</p>
							</div>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="list-left">
							<div class="list-icon">
								<img src="<?php echo base_url('asset_front/images/new/icon_2.png'); ?>" class="img-responsive" alt="Image">
							</div>
							<div class="left-text">
								<h3>02</h3>
								<h4>Minimum Resiko</h4>
								<p>Puyuh lebih tahan perubahan cuaca & minim resiko penyakit banding Ayam.</p>
							</div>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="list-left">
							<div class="list-icon">
								<img src="<?php echo base_url('asset_front/images/new/icon_3.png'); ?>" class="img-responsive" alt="Image">
							</div>
							<div class="left-text">
								<h3>03</h3>
								<h4>Modal Terjangkau</h4>
								<p>Tidak diperlukan modal besar dan lahan yang luas dibanding Ayam Petelur</p>
							</div>
						</div>
					</div>

				</div>
				<div class="col-lg-4">
					<img src="<?php echo base_url('asset_front/images/new/icon_center.png'); ?> " class="img-responsive" alt="Image">
					<div class="btn-alasan">
						<a href="" title=""><img src="<?php echo base_url('asset_front/images/new/btn_gabung.png"'); ?>  class="img-responsive" alt="Image"></a>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="col-lg-12">
						<div class="list-right">
							<div class="right-icon">
								<img src="<?php echo base_url('asset_front/images/new/icon_4.png') ?> " class="img-responsive" alt="Image">
							</div>
							<div class="right-text">
								<h3>04</h3>
								<h4>Permintaan Telur Puyuh Tinggi</h4>
								<p>Konsumsi telur puyuh di wilayah Jawa Tengah & D.I.Yogyakarta tinggi</p>
							</div>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="list-right">
							<div class="right-icon">
								<img src="<?php echo base_url('asset_front/images/new/icon_4.png') ?> " class="img-responsive" alt="Image">
							</div>
							<div class="right-text">
								<h3>05</h3>
								<h4>Afkir Puyuh Banyak Dicari</h4>
								<p>Daging puyuh dimintai pecinta kuliner, permintaan pasar tinggi</p>
							</div>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="list-right">
							<div class="right-icon">
								<img src="<?php echo base_url('asset_front/images/new/icon_4.png') ?> " class="img-responsive" alt="Image">
							</div>
							<div class="right-text">
								<h3>06</h3>
								<h4>Kotoran Bernilai Ekonomis</h4>
								<p>Alternatif pakan ikan dan pupuk kandang (crude protein tinggi 28%-Riset IPB)</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<div class="clearfix" style="background: #FAEDC5"></div>
	<section class="maps-populasi" style="background: #FAEDC5">
		<h3 class="title-alasan">
			Ketua Kelompok & Jumlah Anggota <br> Paguyuban Peternak Puyuh Petelur Kami Di Wilayah Jateng - DIY
		</h3>
		<div class="col-lg-3">
			<div class="col-lg-12">
				<div class="koor1">
					<div class="arrow-pop-1"><img src="<?php echo base_url('asset_front/images/new/asset_lg_right.png'); ?>" class="img-responsive" alt="Image"></div>
					<img src="<?php echo base_url('asset_front/images/new/avatar_tarto.png"'); ?>  class="img-responsive img-circle img-avatar" alt="Image">
					<div class="box-number-populasi">
						<div class="box-alamat">
							<p>Seluruh Pusat Seluruh Wilayah Jateng - DIY</p>
							<h4>Sutarto</h4>
							<span>081328818656</span>
						</div>
						<div class="box-number-kontak">
						</div>
						<div class="col-lg-6">
							<div class="text-peternak">
								<img src="<?php echo base_url('asset_front/images/new/icon_sm1.png'); ?> " class="img-responsive" alt="Image">
								<span class="count-populasi"><?php echo number_format($data_populasi_005->JumPlas,0,',','.'); ?></span>
							</div>
							<div class="text-populasi">
								<img src="<?php echo base_url('asset_front/images/new/icon_sm2.png'); ?> " class="img-responsive" alt="Image">
								<span class="count-populasi"><?php echo number_format($data_populasi_005->Populasi,0,',','.'); ?></span>
							</div>
						</div>
						<div class="col-lg-6">
							<img src="<?php echo base_url('asset_front/images/new/icon_sm3.png'); ?> " class="img-responsive" alt="Image">
							<span class="count-populasi"><?php echo number_format($data_populasi_005->RataSurplus,0,',','.'); ?></span>
							<span class="text-rataminggu">Rata-rata <br> Penghasilan minggu ini</span>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-12">
				<div class="koor2">
					<div class="box-number-populasi1">
						<div class="arrow-pop-2"><img src="<?php echo base_url('asset_front/images/new/asset_lg_right.png'); ?>" class="img-responsive" alt="Image"></div>
						<img src="<?php echo base_url('asset_front/images/new/avatar_sigit.png'); ?> " class="img-responsive img-circle img-avatar1" alt="Image">
						<div class="box-alamat1">
							<p>Wilayah Gombong Kebumen, Banyumas, Cilacap, Wonosobo, Banjarnegara, Purbalingga</p>
							<h4>Sigit Purnomo</h4>
							<span>081804382439</span>
							
						</div>
						<div class="box-number-kontak">
						
						</div>
						<div class="col-lg-6">
							<div class="text-peternak">
								<img src="<?php echo base_url('asset_front/images/new/icon_sm1.png'); ?> " class="img-responsive" alt="Image">
								<span class="count-populasi"><?php echo number_format($data_populasi_008->JumPlas, 0,',','.'); ?></span>
							</div>
							<div class="text-populasi">
								<img src="<?php echo base_url('asset_front/images/new/icon_sm2.png'); ?> " class="img-responsive" alt="Image">
								<span class="count-populasi"><?php echo number_format($data_populasi_008->Populasi, 0,',','.'); ?></span>
							</div>
						</div>
						<div class="col-lg-6">
							<img src="<?php echo base_url('asset_front/images/new/icon_sm3.png'); ?> " class="img-responsive" alt="Image">
							<span class="count-populasi"><?php echo number_format($data_populasi_008->RataSurplus, 0,',','.'); ?></span>
							<span class="text-rataminggu">Rata-rata <br> Penghasilan minggu ini</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<img src="<?php echo base_url('asset_front/images/new/maps_jawa.png'); ?> " class="img-responsive" alt="Image">
		</div>
		<div class="col-lg-3">
			<div class="col-lg-12">
				<div class="koor3">
					<div class="arrow-pop-3"><img src="<?php echo base_url('asset_front/images/new/asset_lg_left.png'); ?>" class="img-responsive" alt="Image"></div>
					<div class="box-populasi-right">
						<img src="<?php echo base_url('asset_front/images/new/avatar_nanang.png'); ?> " class="img-responsive img-circle img-avatar-right" alt="Image">
						<div class="box-alamat-right">
							<p>Wilayag Brosot, Bantul, Kulonprogo, Purworejo, Kutoarjo</p>
							<h4>Nanang S</h4>
							<span>087838288820</span>
						</div>
						<div class="box-number-kontak">
						
						</div>
						<div class="col-lg-6">
							<div class="text-peternak">
								<img src="<?php echo base_url('asset_front/images/new/icon_sm1.png'); ?>" class="img-responsive" alt="Image">
								<span class="count-populasi"><?php echo number_format($data_populasi_001->JumPlas, 0,',','.'); ?></span>
							</div>
							<div class="text-populasi">
								<img src="<?php echo base_url('asset_front/images/new/icon_sm2.png'); ?>" class="img-responsive" alt="Image">
								<span class="count-populasi"><?php echo number_format($data_populasi_001->Populasi, 0,',','.'); ?></span>
							</div>
						</div>
						<div class="col-lg-6">
							<img src="<?php echo base_url('asset_front/images/new/icon_sm3.png'); ?>" class="img-responsive" alt="Image">
							<span class="count-populasi"><?php echo number_format($data_populasi_001->RataSurplus, 0,',','.'); ?></span>
							<span class="text-rataminggu">Rata-rata <br> Penghasilan minggu ini</span>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-12">
				<div class="koor4">
					<div class="box-populasi-right1">
						<div class="arrow-pop-4"><img src="<?php echo base_url('asset_front/images/new/asset_lg_left.png'); ?>" class="img-responsive" alt="Image"></div>
						<img src="<?php echo base_url('asset_front/images/new/avatar_aziz.png'); ?> " class="img-responsive img-circle img-avatar-right" alt="Image">
						<div class="box-alamat-right">
							<p>Wilayah Kalasan, Sleman, Magelang, Klaten, Gunungkidul, Wonosari & Solo</p>
							<h4>Aziz Risdianto</h4>
							<span>085228378830</span>
						</div>
						<div class="box-number-kontak">
						
						</div>
						<div class="col-lg-6">
							<div class="text-peternak">
								<img src="<?php echo base_url('asset_front/images/new/icon_sm1.png'); ?>" class="img-responsive" alt="Image">
								<span class="count-populasi"><?php echo number_format($data_populasi_003->JumPlas, 0,',','.'); ?></span>
							</div>
							<div class="text-populasi">
								<img src="<?php echo base_url('asset_front/images/new/icon_sm2.png'); ?>" class="img-responsive" alt="Image">
								<span class="count-populasi"><?php echo number_format($data_populasi_003->Populasi, 0,',','.'); ?></span>
							</div>
						</div>
						<div class="col-lg-6">
							<img src="<?php echo base_url('asset_front/images/new/icon_sm3.png'); ?>" class="img-responsive" alt="Image">
							<span class="count-populasi"><?php echo number_format($data_populasi_003->RataSurplus, 0,',','.'); ?></span>
							<span class="text-rataminggu">Rata-rata <br> Penghasilan minggu ini</span>
						</div>
					</div>
				</div>
			</div>
	</section>

	<div class="clearfix" style="background: #FAEDC5"></div>
	<section class="steep-ternak">
		<div class="col-lg-4">
			<div class="box-tahapan">
				<p>
					Tahapan - tahapan Berternak Bersama Guyubingmanah
				</p>
			</div>
			<img src="<?php echo base_url('asset_front/images/new/img_body_left.png'); ?>" class="img-responsive" alt="Image">
		</div>
		<div class="col-lg-8">
			
			<div class="col-lg-4 mas-1">
				<div class="arrow">
					<img src="<?php echo base_url('asset_front/images/new/asset_arraw_1.png'); ?>" class="img-responsive" alt="Image">
				</div>
				<img src="<?php echo base_url('asset_front/images/new/mascot_steep_1.png'); ?>" class="img-responsive" alt="Image">
				<div class="box-text">
					<p>
						Silahkan hubungi ketua kelompok daerah Anda untuk mendapatkan informasi detail tentang kemitraan puyuh petelur Guyubingmanah.
					</p>
				</div>
				<div class="btn-box">
					<a class="btn btn-lg btn-kontak btn-success" href="#" role="button">Konsultasi Sekarang</a>
				</div>
				
			</div>
			<div class="col-lg-4 mas-2">
				<div class="arrow-2">
					<img src="<?php echo base_url('asset_front/images/new/asset_arraw_1.png'); ?>" class="img-responsive" alt="Image">
				</div>
				<img src="<?php echo base_url('asset_front/images/new/mascot_steep_2.png'); ?>" class="img-responsive" alt="Image">
				<div class="box-text2">
					<p>
						Pendamping kelompok akan memberikan gambaran kebutuhan ternak (kandang, perlengkapan inum, dsb) demi keberhasilan ternak puyuh Anda.
					</p>
				</div>
				<div class="btn-box2">
					<a class="btn btn-lg btn-kontak btn-success" href="#" role="button">Konsultasi Sekarang</a>
				</div>
			</div>
			<div class="col-lg-4 mas-3">
				<img src="<?php echo base_url('asset_front/images/new/mascot_steep_3.png'); ?>" class="img-responsive" alt="Image">
				<div class="box-text3">
					<p>
						DOQ dan pakan akan kami kirim ke kandang Anda sesuai dengan jadwal yang telah disepakati.
					</p>
				</div>
				<div class="btn-box3">
					<a class="btn btn-lg btn-kontak btn-success" href="#" role="button">Konsultasi Sekarang</a>
				</div>
			</div>

		</div>
	</section>

	<div class="clearfix" style="background: #FAEDC5"></div>
	<div class="testimoni" style="background-image: url('<?php echo base_url('asset_front/images/new/bg_testimoni.png'); ?>')">
		<h3>Apa Kata Peternak Kami</h3>

		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					
					<div id="carousel-id" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#carousel-id" data-slide-to="0" class="active"></li>
							<li data-target="#carousel-id" data-slide-to="1" class=""></li>
						</ol>
						<div class="carousel-inner">
							<div class="item active">
								<div class="col-md-4 col-lg-4">
									<div class="box-testimoni">
										Saya bergabung menjadi sejak 
										tahun 2000, dengan populasi 
										500 ekor, selanjutanya 3000 
										ekor karena keterbatasan 
										lahan.
										<br>
										Dengan menjadi anggota, 
										harga telur stabil, perawatan 
										puyuh terbilang mudah 
										sehingga tidak mengganggu 
										aktifitas utama di sawah.
										<br>
										SUGIMAN (40 th)<br>
										Petani<br>
										Cangkringan, Sleman<br>
									</div>
									<div class="img-avatar-testimoni">
										<img src="<?php echo base_url('asset_front/images/new/ava_testi_1.png'); ?>" class="img-responsive" alt="Image">
									</div>
								</div>
								<div class="col-md-4 col-lg-4">
									<div class="box-testimoni">
										Info awal dari teman yang 
										telah sukses, tahun 2016 
										memutuskan bergabung 
										dengan populasi awal 
										5.000 ekor.<br>

										Benefit menjanjikan, tidak 
										mengganggu aktifitas 
										utama, saya tidak perlu 
										khawatir masalah telur 
										karena sudah diambil. <br>

										M. DANU WARDANA<br>
										Usaha Cuci Mobil<br>
										Hargomulyo, Sleman
									</div>
									<div class="img-avatar-testimoni">
										<img src="<?php echo base_url('asset_front/images/new/ava_testi_2.png'); ?>" class="img-responsive" alt="Image">
									</div>
								</div>
								<div class="col-md-4 col-lg-4">
									<div class="box-testimoni">
										Bergabung dengan paguyuban 
										ditahun 2009 dengan populasi 
										2.800 ekor.<br>

										Dengan beternak puyuh saya 
										dapat menghemat pakan  ikan 
										karena kotoran puyuh dapat 
										dimanfaatkan menjadi pakan 
										ikan, sehingga saya untung 
										dobel, panen telur dan juga 
										panen ikan.<br>

										NUHADI (49th)<br>
										Petani Ikan<br>
										Cangkringan, Sleman.
									</div>
									<div class="img-avatar-testimoni">
										<img src="<?php echo base_url('asset_front/images/new/ava_testi_3.png'); ?>" class="img-responsive img-circle" alt="Image">
									</div>
								</div>
							</div>
							<div class="item">
								<div class="col-md-4 col-lg-4">
									<div class="box-testimoni">
										Sejak tahun 2012 bergabung, karena teman 
										saya yang juga Mantan TKI lebih dahulu 
										bergabung dan sukses beternak puyuh.<br>

										Saya yang awam didunia peternakan, 
										didampingi paguyuban mulai dari pembuatan 
										kandang, pemeliharaan hingga pengelolaan 
										limbah.<br>

										Awal beternak 2000 ekor, bulan ke empat 
										menjadi 4000 ekor, hingga sekarang 10.000 
										ekor.<br>

										WARDI KUSNANTO (31 th)<br>
										Mantan TKI Malaysia<br>
										Temon, Kulonprogo
									</div>
									<div class="img-avatar-testimoni">
										<img src="<?php echo base_url('asset_front/images/new/ava_testi_4.png'); ?>" class="img-responsive" alt="Image">
									</div>
								</div>
								<div class="col-md-4 col-lg-4">
									<div class="box-testimoni">
										Awal tahun 2014 saya mulai 
										bergabung menjadi peternak Puyuh.<br>

										Lahan saya hanya 7x10 meter, mampu 
										menampung 4.000 ekor,
										jadi jangan khawatir jika teman-teman 
										tidak mempunyai lahan luas.<br>

										Selama Bergabung, jika ada masalah 
										apapun tentang puyuh, tinggal telp 
										langsung datang pembimbing 
										peternak dari paguyuban.<br>

										ANDI (40 th)<br>
										Tukang Batu<br>
										Ngemplak, Sleman
									</div>
									<div class="img-avatar-testimoni">
										<img src="<?php echo base_url('asset_front/images/new/ava_testi_5.png'); ?>" class="img-responsive" alt="Image">
									</div>
								</div>
								<div class="col-md-4 col-lg-4">
									<div class="box-testimoni">
										Sebelumnya Saya beternak Ayam & 
										Sapi namun semua Gagal.
										Di tahun 2015 saya bergabung, 
										dengan populasi awal 2.200 ekor , 
										tahun ketiga menjadi 14.200 ekor<br>

										Puyuh tidak membutuhkan lahan luas, 
										modal kecil, tetapi lebih untung<br>

										Servis Paguyuban luar biasa, Bibit 
										berkualitas, pakan diantar sampe 
										kandang, dan telur diambil.<br>

										IHSANUDIN<br>
										Pegawai<br>
										Wates, Kulonprogo
									</div>
									<div class="img-avatar-testimoni">
										<img src="<?php echo base_url('asset_front/images/new/ava_testi_6.png'); ?>" class="img-responsive img-circle" alt="Image">
									</div>
								</div>
							</div>
							
						</div>
					</div>
	
				</div>
			</div>
		</div>
	</div>

	<div class="clearfix"></div>
	<div class="footer">
		<div class="col-md-3 footer-left-agileits">
			<img src="<?php echo base_url('asset_front/images/new/logo_guyup_2.png'); ?> " class="img-responsive" alt="Image">
		</div>
		<div class="col-md-3 footer-left-agileinfo">
			<h3>Tentang Kami</h3>
			<ul class="footer-tentang">
				<li>Kantor : Pulo, Brosot, Galur, Kulon Progo</li>
				<li>Telp : 0274-2850064</li>
				<li>Email : guyubingmanah@gmail.com</li>
				<li>Hotline : 081328818656</li>
			</ul>
		</div>
		<div class="col-md-3 footer-left-agileinfo">
			<h3>Service Kami</h3>
			<ul class="footer-tentang">
				<li><a href="" title="">Beranda</a></li>
				<li><a href="" title="">Tentang Kami</a></li>
				<li><a href="" title="">Daftar</a></li>
				<li><a href="" title="">Tips</a></li>
				<li><a href="" title="">FAQ</a></li>
				<li><a href="" title="">Kontak</a></li>
			</ul>
		</div>
		<div class="col-md-3 footer-left-w3-agileits">
			<h3>Follow Akun Kami:</h3>
				<ul class="social-icons">
					<li><a href="#" class="icon icon-border facebook"></a></li>
					<li><a href="#" class="icon icon-border instagram"></a></li>
				</ul>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="copyright-w3-agile">
		<div class="container">
			<p>© 2018 Guyubingmanah | Web Developer</p>
			<div class="clearfix"> </div>
		</div>
	</div>
	

<!-- //banner -->

<?php require_once(APPPATH .'views/include/front/front_inc_script.php'); ?>

</body>
</html>