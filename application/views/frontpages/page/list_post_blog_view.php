<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once(APPPATH .'views/include/front/front_inc_style.php'); ?>
</head>
<body>
	<?php $this->load->view('frontpages/menu_nav'); ?>
	
	<section class="konten-forum">
		<div class="container">
			
			<?php foreach ($data_konten as $dt_konten): ?>
				
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="well">
				      <div class="media">
				      	
				      	<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				      		<a href="<?php echo base_url('konten/'.$dt_konten->slug_url) ?>">
					    		<img class="img-thumbnail img-responsive" src="<?php echo base_url('storage_img/thubnail/'.$dt_konten->file_name); ?>">
					  		</a>
				      	</div>
				      	<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
				      		<div class="media-body">
					    		<h4 class="media-heading"><a href="<?php echo base_url('konten/'.$dt_konten->slug_url); ?>" title="<?php echo $dt_konten->title_konten ?>"><?php echo $dt_konten->title_konten; ?></a></h4>
					          <p class="text-right">By Admin</p>
					          <?php $this->load->model('Lib_model'); $this->load->model('Command_model'); $m_lib = new Lib_model(); $m_command = new Command_model(); ?>
					          <p> <?php echo $m_lib->cutText($dt_konten->desc_konten, 600); ?> </p>
					          <ul class="list-inline list-unstyled">
					  			<li><span><i class="glyphicon glyphicon-calendar"></i> <?php echo $m_lib->time_elapsed_string($dt_konten->tgl_create); ?> </span></li>
					            <li>|</li>
					            <span><a href="<?php echo base_url('konten/'.$dt_konten->slug_url) ?>" title=""></a><i class="glyphicon glyphicon-comment"></i> <?php echo $m_command->count_data_command_by_kode_kontent('Tbl_Comment', $dt_konten->kode_konten) ?> comments</span>
					            <li>|</li>
								</ul>
					       </div>
				      	</div>

				    </div>
				  </div>
				</div>

			<?php endforeach ?>

		</div>
	</section>

	<?php //$this->load->view('frontpages/footer'); ?>

	<?php require_once(APPPATH .'views/include/front/front_inc_script.php'); ?>

</body>
</html>