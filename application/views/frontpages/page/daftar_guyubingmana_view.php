<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once(APPPATH .'views/include/front/front_inc_style.php'); ?>
</head>
<body>
	<?php $this->load->view('frontpages/menu_nav'); ?>

	<section class="daftar-guyu">
		<div class="container-fluid">
			<div class="row">
				<div class="title-daftar">
					<h2>Form Pendaftaran Guyubingmanah</h2>
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<div class="daftar-icon-maskot">
							<img src="<?php echo base_url('asset_front/images/mascot_puyuh.png'); ?>" class="img-responsive" alt="Image">
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<div class="keterangan-daftar">
							<h2>Selamat Datang, Calon Peternak Puyuh Sukses.</h2>
							<p>
								Silahkan isikan data Anda di kolom pendaftaran
								untuk selanjutnya akan dikonfirmasi oleh Koordinator kami.
							</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
					<?php if (isset($_SESSION['message_data'])): ?>
                      	<div class="alern-berhasil">
							<p>
								<?php echo $_SESSION['message_data'] ?>
							</p>
						</div>
                     <?php endif ?>

                    <?php if (isset($_SESSION['error_data'])): ?>
                      	<div class="alern-gagal">
							<p>
								<?php echo $_SESSION['error_data'] ?>
							</p>
						</div>
                    <?php endif ?>
					<form action="<?php echo base_url('Front/action_register_member'); ?>" method="POST" role="form">
					
						<div class="form-group">
							<label><i class="fa fa-user"></i> Nama Lengkap <sup style="color:red">*</sup></label>
							<input type="text" name="nama_lengkap" class="form-control" id="" placeholder="Nama Lengkap">
						</div>
						<div class="form-group">
							<label><i class="fa fa-phone"></i> Nomor Telepon / Wa (Aktif) <sup style="color:red">*</sup></label>
							<input type="text" name="no_hp" class="form-control" id="" placeholder="Nomor Pastikan Aktif">
						</div>
						<div class="form-group">
							<label><i class="fa fa-envelope"></i> Email </label>
							<input type="email" name="email" class="form-control" id="" placeholder="Kosongkan Jika Tidak Memiliki Email">
						</div>
						<div class="form-group">
							<label><i class="fa fa-map"></i> Alamat Rumah <sup style="color:red">*</sup></label>
							<input type="text" name="alamat_rumah" class="form-control" id="" placeholder="Alamat Rumah Sekarang">
						</div>
						<div class="form-group">
							<label><i class="fa fa-suitcase"></i> Pekerjaan Saat Ini <sup style="color:red">*</sup></label>
							<input type="text" name="pekerjaan" class="form-control" id="" placeholder="Pekerjaan Saat Ini">
						</div>
						<div class="form-group">
							<label>Alasan Ikut Guyubingmanah <sup style="color:red">*</sup></label>
							<textarea name="alasan_ternak" id="" class="form-control" rows="3" required="required"></textarea>
						</div>
						<div class="form-group">
							<label>Apakah Anda sudah pernah beternak sebelumnya? <sup style="color:red">*</sup></label>
							<div class="radio">
								<label>
									<input type="radio" name="status_ternak" id="input" value="Sudah" checked="checked">
									Sudah
								</label>
								<label>
									<input type="radio" name="status_ternak" id="input" value="Belum">
									Belum
								</label>
							</div>
						</div>
					
						<button type="submit" class="btn btn-block btn-primary">DAFTAR GUYUBINGMANAH</button>
					</form>
				</div>
			</div>
		</div>
	</section>


	<?php $this->load->view('frontpages/footer'); ?>

	<?php require_once(APPPATH .'views/include/front/front_inc_script.php'); ?>

</body>
</html>