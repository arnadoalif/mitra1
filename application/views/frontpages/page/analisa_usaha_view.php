<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once(APPPATH .'views/include/front/front_inc_style.php'); ?>
</head>
<body>
	<?php $this->load->view('frontpages/menu_nav'); ?>
	
	<section class="header-analisa">
		<div class="head-analisa">
			<img src="<?php echo base_url('asset_front/images/head_analisa_bisnis.png'); ?>" class="img-responsive" alt="Image">
			<div class="logo-maskot-analisa">
				<img src="<?php echo base_url('asset_front/images/mascot_profile.png'); ?>" class="img-responsive" alt="Image">
				<div class="title-analisa">
					<h3>Analisa Usaha</h3>
					<p>Peternakan Puyuh Petelur</p>
				</div>
			</div>
		</div>
	</section>

	<section class="table-analisa">
		<div class="container-fluid">
			<div class="table-title-analisa">
				<p>
					Analisa Usaha Peternakan Puyuh Petelur Skala 2.000 ekor berdasarkan kajian Data yang Valid.* Mulai Dari Modal, Investasi Dan Keuntungan.
				</p>
			</div>
			<div class="tabel-kandang-sangkar">
				<div class="hidden-xs hidden-sm col-md-4 col-lg-4">
					<div class="img-kandang">
						<div class="text-investasi">
							<h3>Investasi</h3>
						</div>
						<img src="<?php echo base_url('asset_front/images/img_kandang.png'); ?>" class="img-responsive" alt="Image">
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title">Kandang Sangkar</h3>
						</div>
						<div class="panel-body">
							<table class="table table-responsive table-hover table-bordered">
								<thead>
									<tr>
										<th>Kandang, kapasitas @165 ekor</th>
										<th>= Rp.570.000</th>
									</tr>
									<tr>
										<th>Untuk 2000 ekor</th>
										<th>= Rp.12 x Rp. 570.000</th>
									</tr>
									<tr>
										<th></th>
										<th>= Rp.6.840.000</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td colspan="2">Penyusutan kandang 20% / Tahun</td>
									</tr>
									<tr>
										<td colspan="2">Kandang bisa digunakan untuk 5 tahun</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="bibit-puyuh-doq">
				<div class="hidden-xs hidden-sm col-md-4 col-lg-4">
					<div class="img-bibit-puyuh-doq">
						<div class="img-title-puyuh">
							<h3>Modal</h3>
						</div>
						<img src="<?php echo base_url('asset_front/images/img_bibit_doq.png'); ?>" class="img-responsive" alt="Image">
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title">Bibit Puyuh / DOQ</h3>
						</div>
						<div class="panel-body">
							<img src="<?php echo base_url('asset_front/images/img_table_biaya_bep.png'); ?>" class="img-responsive" alt="Image">
							<!-- <table class="hidden-xs hidden-sm table table-responsive table-bordered">
								<thead>
									<tr>
										<th>Tahap</th>
										<th>DOQ Datang</th>
										<th>Jumlah</th>
										<th>Satuan</th>
										<th>Harga</th>
										<th>Total Harga</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="5">Tahap 1</td>
									</tr>
									<tr>
										<td>DOQ</td>
										<td>2.000</td>
										<td>Ekor</td>
										<td align="right">Rp 2.300</td>
										<td align="right">Rp 4.600.000</td>
									</tr>
									<tr>
										<td>Pakar starter (BR) giling</td>
										<td>3</td>
										<td>Zak</td>
										<td align="right">Rp 344.000</td>
										<td align="right">Rp 1.032.000</td>
									</tr>
									<tr>
										<td>P. Chick (Vitamin Pertumbuhan)</td>
										<td>6</td>
										<td>Bungus</td>
										<td align="right">Rp 17.000</td>
										<td align="right">Rp 102.000</td>
									</tr>
									<tr>
										<td>P. Bio (Vitamin anti stress)</td>
										<td>3</td>
										<td>Bungus</td>
										<td align="right">Rp 13.000</td>
										<td align="right">Rp 39.000</td>
									</tr>
									<tr>
										<td colspan="6" align="right"> Rp 5.773.000 </td>
									</tr>

									<tr>
										<td rowspan="5">Tahap 2</td>
									</tr>
									<tr>
										<td>Fase Starter</td>
										<td></td>
										<td></td>
										<td align="right"></td>
										<td align="right"></td>
									</tr>
									<tr>
										<td>Pakan starter s/d umur 35 hari</td>
										<td>16</td>
										<td>Zak</td>
										<td align="right">Rp 334.000</td>
										<td align="right">Rp 5.344.000</td>
									</tr>
									<tr>
										<td>Vaksin ND</td>
										<td>2</td>
										<td>Botol</td>
										<td align="right">Rp 17.000/td>
										<td align="right">Rp 34.000</td>
									</tr>
									<tr>
										<td>Vaksin AI</td>
										<td>2.000</td>
										<td>Ekor</td>
										<td align="right">Rp 175</td>
										<td align="right">Rp 350.000</td>
									</tr>
									<tr>
										<td colspan="6" align="right"> Rp 5.728.000</td>
									</tr>

									<tr>
										<td rowspan="5">Tahap 3</td>
									</tr>
									<tr>
										<td>Fase Pindah Kandang</td>
										<td></td>
										<td></td>
										<td align="right"></td>
										<td align="right"></td>
									</tr>
									<tr>
										<td>Pakan Layer umur 35 s/d umur 42 hari</td>
										<td>8</td>
										<td>Zak</td>
										<td align="right">Rp 285.000</td>
										<td align="right">Rp 2.280.000</td>
									</tr>
									<tr>
										<td>Vit C/anti stress</td>
										<td>2</td>
										<td>Bungkus</td>
										<td align="right">Rp 7.000</td>
										<td align="right">Rp 14.000</td>
									</tr>
									<tr>
										<td colspan="6" align="right"> Rp 2.294.000 </td>
									</tr>

									<tr>
										<td rowspan="5">Tahap 4</td>
									</tr>
									<tr>
										<td>Fase Bertelur</td>
										<td></td>
										<td></td>
										<td align="right"></td>
										<td align="right"></td>
									</tr>
									<tr>
										<td>Pakan Layer umur 43 s/d BEP</td>
										<td>8</td>
										<td>Zak</td>
										<td align="right">Rp 285.000</td>
										<td align="right">Rp 3.990.000</td>
									</tr>
									<tr>
										<td>Vit C/anti stress</td>
										<td>6</td>
										<td>Bungkus</td>
										<td align="right">Rp 7.000</td>
										<td align="right">Rp 42.000</td>
									</tr>
									<tr>
										<td colspan="6" align="right"> Rp 4.032.000 </td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<td></td>
										<td colspan="1">Total Biaya s/d BEP Per 2000 Ekor </td>
										<td colspan="4" align="right"> Rp 17.827.000</td>
									</tr>
								</tfoot>
							</table> -->
						</div>
					</div>
				</div>
			</div>

			<div class="biaya-tambahan-umur">
				<div class="hidden-xs hidden-sm col-md-4 col-lg-4">
					<div class="img-biaya-tambahan-umur">
						<div class="img-biaya-lain">
							<h3>Biaya Lain</h3>
						</div>
						<img src="<?php echo base_url('asset_front/images/img_biaya_lain.png'); ?>" class=" img-responsive" alt="Image">
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title">Biaya Tambahan Umur BEP s/d 420 hari</h3>
						</div>
						<div class="panel-body">
							<img src="<?php echo base_url('asset_front/images/total_biaya_bep_40.png'); ?>" class="hidden-lg hidden-md img-responsive" alt="Image">
							<table class="hidden-xs hidden-sm table table-hover table-responsive table-bordered">
								<thead>
									<tr>
										<td></td>
										<td>Jumlah</td>
										<td>Satuan</td>
										<td>Harga</td>
										<td>Total Harga</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Vaksin ND</td>
										<td>12</td>
										<td>Botol</td>
										<td align="right">Rp 17.000</td>
										<td align="right">Rp 204.000</td>
									</tr>
									<tr>
										<td>PG Anti Strees</td>
										<td>80</td>
										<td>Bungkus</td>
										<td align="right">Rp 7.000</td>
										<td align="right">Rp 560.000</td>
									</tr>
									<tr>
										<td>P. Bio/Vitamin C</td>
										<td>16</td>
										<td>Bungkus</td>
										<td align="right">Rp 28.000</td>
										<td align="right">Rp 448.000</td>
									</tr>
									<tr>
										<td colspan="6" align="right">Rp 2.252.000</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			<div class="biaya-tunai">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title">Biaya Tunai</h3>
						</div>
						<div class="panel-body">
							<img src="<?php echo base_url('asset_front/images/img_table_biaya.png'); ?>" class="img-responsive" alt="Image">
						</div>
					</div>
				</div>
			</div>

			<div class="keuntungan">
				<div class="hidden-xs hidden-sm col-md-4 col-lg-4">
					<div class="img-keuntungan">
						<div class="img-text-keuntungan">
							<h3>Keuntungan</h3>
						</div>
						<img src="<?php echo base_url('asset_front/images/img_keuntungan.png'); ?>" class="img-responsive" alt="Image">
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title">Keuntungan</h3>
						</div>
						<div class="panel-body">
							<img src="<?php echo base_url('asset_front/images/img_hasil_akhir.png'); ?>" class="img-responsive" alt="Image">
						</div>
					</div>
				</div>
			</div>

			<div class="text-keterangan">
				<p>* Harga setiap kebutuhan diatas dapat berubah sewaktu-waktu mengikuti harga dari produsen.</p>
			</div>
		</div>
	</section>

	<section class="kontak-koordinator-profil">
		<div class="img-kontak">
			<img src="<?php echo base_url('asset_front/images/asset_profile.png'); ?>" class="img-responsive" alt="Image">

			<div class="profil-kontak">
				<div class="container">
					<div class="row">
						<div class="hidden-xs col-sm-12 col-md-4 col-lg-4">
							<div class="img-maskot-kontak">
								<div class="lingkaran">
									<p>Ingin Bergabung ?</p>
								</div>
								<img src="<?php echo base_url('asset_front/images/mascot_profile.png'); ?>" class="img-responsive" alt="Image">
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<div class="daftar-kontak-profil">
									<div class="title-wilayah">
										<p>
											<span class="bold-title-wilayah">Pimpinan Pusat</span><br>
											Pulo, Brosot, Galur, Kulon Progo
											(0274) 2850064
										</p>
									</div>
									<br>
									<div class="nama-koordinator">
										<p>
											<strong> Sutarto </strong> <br>
											081328818656
										</p>
									</div>
								</div>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<div class="daftar-kontak-profil">
									<div class="title-wilayah">
										<p>
											<span class="bold-title-wilayah">Wilayah Kulonprogo </span><br>
											Area : Bantul, Kulonprogo, Purworejo, Kutoarjo
										</p>
									</div>
									<br>
									<div class="nama-koordinator">
										<p>
											<strong>Nanang S</strong> <br>
											087838288820
										</p>
									</div>
								</div>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<div class="daftar-kontak-profil">
									<div class="title-wilayah">
										<p>
											<span class="bold-title-wilayah">Wilayah Kebumen</span><br>
											Area : Kebumen, Banyumas, Cilacap, Wonosobo, Banjarnegara, Purbalingga
										</p>
									</div>
									<br>
									<div class="nama-koordinator">
										<p>
											<strong> Sigit Purnomo </strong> <br>
											081804382439
										</p>
									</div>
								</div>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<div class="daftar-kontak-profil">
									<div class="title-wilayah">
										<p>
											<span class="bold-title-wilayah">Wilayah Sleman </span><br>
											Area : Sleman, Magelang, Klaten, Gunungkidul, Wonogiri dan Solo
										</p>
									</div>
									<br>
									<div class="nama-koordinator">
										<p>
											<strong> Aziz Risdianto </strong> <br>
											085228378830
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</section>

	<?php $this->load->view('frontpages/footer'); ?>

	<?php require_once(APPPATH .'views/include/front/front_inc_script.php'); ?>

</body>
</html>