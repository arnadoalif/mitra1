<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once(APPPATH .'views/include/front/front_inc_style.php'); ?>
</head>
<body>
	<?php $this->load->view('frontpages/menu_nav'); ?>

	<section class="header-profil">
		<div class="head-img">
			<img src="<?php echo base_url('asset_front/images/head_profile.png'); ?> " class="img-responsive" alt="Image">
			<div class="logo-maskot-profil">
				<img src="<?php echo base_url('asset_front/images/mascot_profile.png'); ?> " class="img-responsive" alt="Image">
				<div class="title-profil">
					<h3>Tentang Guyubingmanah</h3>
				</div>
			</div>
		</div>
		
		<div class="profil-guyubingmana">
			
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<div class="deskripsi-profil">
							<p>
								<strong>GUYUBINGMANAH</strong> adalah Paguyuban Dan Kemitraan Puyuh Petelur terbesar di provinsi Jawa Tengah dan D.I. Yogyakarta yang telah berdiri sejak tahun 2000 dengan pimpinan pusat berada di Brosot, Kulonprogo, Yogyakarta.

								Awal berdirinya paguyuban karena peternak puyuh masih terkendala ketersediaan bibit puyuh yang berkualitas, pendistribusian pakan yang belum merata serta pemasaran telur yang masih lokal sehingga harga telur belum stabil.
								
								Dengan bergabung menjadi anggota Guyubingmanah, para peternak tidak perlu khawatir dalam mencari bibit dan pakan, karena paguyuban telah menjalin kerjasama dengan pabrik penghasil bibit DOQ dan pabrik pakan sehingga keberlangsungan usaha peternakan Anda terjamin.
							</p>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<div class="deskripsi-profil">
							<p>
								Untuk mempermudah pendampingan, kami telah membagi wilayah paguyuban peternak kami menjadi 4 wilayah yaitu :
								<ol>
								    <li>Wilayah Pusat <br>
										Meliputi seluruh wilayah Jawa Tengah utara- barat.
									</li>
								    <li>
								    	Wilayah Gombong <br>
										Meliputi daerah Kebumen, Banyumas, Cilacap, Wonosobo, Banjarnegara, dan Purbalingga
								    </li>
								    <li>
								    	Wilayah Brosot <br>
										Meliputi daerah Bantul, Kulonprogo, dan Purworejo.
								    </li>
								    <li>
								    	Wilayah Kalasan <br>
									Meliputi daerah Sleman, Magelang, Klaten, Gunungkidul, Wonogiri dan Solo.
								    </li>
								</ol>
							</p>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<div class="deskripsi-profil">
							<p>
								Dengan berpengalaman lebih dari 17 tahun membimbing anggota guyubingmanah, kami siap menjadikan anda peternak puyuh sukses berikutnya. Jumlah anggota paguyuban kami saat ini lebih dari 500 peternak aktif dengan populasi lebih dari 1 juta ekor.

								Segera gabung bersama kami, Anda akan kami bimbing mulai dari tahap awal hingga BERHASIL menjadi peternak yang tangguh dan sukses.
							</p>	
						</div>
					</div>
					<div class="img-puyuh-profil">
						<img src="<?php echo base_url('asset_front/images/img_puyuh_profile.png'); ?> " class="img-responsive img-circle" alt="Image">
					</div>
				</div>
			</div>

		</div>
		
	</section>

	<section class="keuntungan-profil">
		<div class="title-keuntungan-berternak">
			<h3>Keuntungan Berternak Bersama Guyubingmanah</h3>
		</div>
		<div class="box-keuntungan-profil">
			<div class="img-telur-left">
				<img src="<?php echo base_url('asset_front/images/img_telur_profile.png'); ?>" class="img-responsive hidden-sm hidden-xs" alt="Image">
			</div>

			<div class="list-keuntungan-ternak">
				<div class="container">
					<div class="row">
						<div class="hidden-xs hidden-sm col-md-6 col-lg-6">
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div class="icon-img">
								<img src="<?php echo base_url('asset_front/images/asset_profil_1.png'); ?>" class="img-responsive" alt="Image">
							</div>
							<div class="title-list-keuntungan">
								<h3>Pendampingan Usaha</h3>
								<div class="deskripsi-keuntungan">
									<p>	
										Kami berikan Konsultasi & Bimbingan GRATIS,
										mulai dari tahap awal hingga akhir periode. 
									</p>
								</div>
							</div>
						</div>
						<div class="hidden-xs hidden-sm col-md-6 col-lg-6">
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div class="icon-img">
								<img src="<?php echo base_url('asset_front/images/asset_profil_2.png'); ?>" class="img-responsive" alt="Image">
							</div>
							<div class="title-list-keuntungan">
								<h3>Keberlangsungan Investasi</h3>
								<div class="deskripsi-keuntungan">
									<p>			
										Kami juga memberikan manajemen pengelolaan 
										keuangan agar hasil beternak Anda maksimal.
									</p>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div class="icon-img">
								<img src="<?php echo base_url('asset_front/images/asset_profil_4.png'); ?>" class="img-responsive" alt="Image">
							</div>
							<div class="title-list-keuntungan">
								<h3>Bibit Puyuh Berkualitas</h3>
								<div class="deskripsi-keuntungan">
									<p>									
										Bibit Puyuh 99% Betina, Puncak Produksi Tinggi 
										Hingga 94%, Kematian Rendah & Masa Produksi 
										Panjang.
									</p>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div class="icon-img">
								<img src="<?php echo base_url('asset_front/images/asset_profil_3.png'); ?>" class="img-responsive" alt="Image">
							</div>
							<div class="title-list-keuntungan">
								<h3>Hasil Panen Tidak Mengikat</h3>
								<div class="deskripsi-keuntungan">
									<p>					
										Anda berhak menjual telur dan afkir sendiri
										tanpa lewat Kemitraan.
									</p>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div class="icon-img">
								<img src="<?php echo base_url('asset_front/images/asset_profil_6.png'); ?>" class="img-responsive" alt="Image">
							</div>
							<div class="title-list-keuntungan">
								<h3>Hasil Panen Di Ambil Paguyuban</h3>
								<div class="deskripsi-keuntungan">
									<p>	
										Telur ditampung oleh paguyuban, 
										telur kami beli dalam kiloan bukan butiran.
									</p>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div class="icon-img">
								<img src="<?php echo base_url('asset_front/images/asset_profil_5.png'); ?>" class="img-responsive" alt="Image">
							</div>
							<div class="title-list-keuntungan">
								<h3>Pakan Berkualitas</h3>
								<div class="deskripsi-keuntungan">
									<p>								
										Pakan Pabrikan berkualitas  (protein 21%) & 
										diantar sampai kandang/semaksimal armada 
										dapat menjangkau.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php $this->load->view('frontpages/kontak_view'); ?>

	<?php $this->load->view('frontpages/footer'); ?>

	<?php require_once(APPPATH .'views/include/front/front_inc_script.php'); ?>

</body>
</html>