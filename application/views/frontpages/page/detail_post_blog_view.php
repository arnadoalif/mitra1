<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once(APPPATH .'views/include/front/front_inc_style.php'); ?>
<style type="text/css">
	input, textarea {
  outline: none;
  border: none;
  display: block;
  margin: 0;
  padding: 0;
  -webkit-font-smoothing: antialiased;
  font-family: "PT Sans", "Helvetica Neue", "Helvetica", "Roboto", "Arial", sans-serif;
  font-size: 1rem;
  color: #555f77;
}
input::-webkit-input-placeholder, textarea::-webkit-input-placeholder {
  color: #ced2db;
}
input::-moz-placeholder, textarea::-moz-placeholder {
  color: #ced2db;
}
input:-moz-placeholder, textarea:-moz-placeholder {
  color: #ced2db;
}
input:-ms-input-placeholder, textarea:-ms-input-placeholder {
  color: #ced2db;
}

p {
  line-height: 1.3125rem;
}

.comments {
  margin: 2.5rem auto 0;
  max-width: 60.75rem;
  padding: 0 1.25rem;
}

.comment-wrap {
  margin-bottom: 1.25rem;
  display: table;
  width: 100%;
  min-height: 5.3125rem;
}

.photo {
  padding-top: 0.625rem;
  display: table-cell;
  width: 3.5rem;
}
.photo .avatar {
  height: 2.25rem;
  width: 2.25rem;
  border-radius: 50%;
  background-size: contain;
}

.comment-block {
  padding: 1rem;
  background-color: #fff;
  display: table-cell;
  vertical-align: top;
  border-radius: 0.1875rem;
  box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.08);
}
.comment-block textarea {
  width: 100%;
  resize: none;
}

.comment-text {
  margin-bottom: 1.25rem;
}

.bottom-comment {
  color: #acb4c2;
  font-size: 0.875rem;
}

.comment-date {
  float: left;
}

.comment-actions {
  float: right;
}
.comment-actions li {
  display: inline;
  margin: -2px;
  cursor: pointer;
}
.comment-actions li.complain {
  padding-right: 0.75rem;
  border-right: 1px solid #e1e5eb;
}
.comment-actions li.reply {
  padding-left: 0.75rem;
  padding-right: 0.125rem;
}
.comment-actions li:hover {
  color: #0095ff;
}
</style>
</head>
<body>
	<?php $this->load->view('frontpages/menu_nav'); ?>
	
	<section class="konten-forum">
		<div class="container">
			
			<div class="well"> 
		        <div class="row">
		             <div class="col-md-12">
		                 <div class="row hidden-md hidden-lg"><h1 class="text-center" ><?php echo $detail_forum->title_konten ?></h1></div>
		                     
		                 <div class="pull-left col-md-4 col-xs-12 thumb-contenido"><img class="center-block img-responsive" src='<?php echo base_url('storage_img/thubnail/'.$detail_forum->file_name); ?>' /></div>
		                 <div class="">
		                     <h1  class="hidden-xs hidden-sm"><?php echo $detail_forum->title_konten ?></h1>
		                     <hr>
		                     <?php $this->load->model('Lib_model'); $m_lib = new Lib_model(); ?>
		                     <small><?php echo $m_lib->time_elapsed_string($detail_forum->tgl_create); ?></small><br>
		                     <small><strong>Author : Admin</strong></small>
		                     <hr>
		                     <p class="text-justify"> <?php echo $detail_forum->desc_konten; ?> 
		                 </div>
		             </div>

		             <div class="col-md-12">
		             	<hr>
	             		<div class="comments">
			
								<?php $m_lib = new Lib_model(); ?>
								<?php foreach ($data_comment as $dt_comment): ?>
									
									<div class="comment-wrap">
										<div class="photo">
												<div class="avatar" style="background-image: url('https://cdn0.iconfinder.com/data/icons/social-messaging-ui-color-shapes/128/chat-circle-blue-512.png')"></div>
										</div>
										<div class="comment-block">
												<label for=""><?php echo $dt_comment->nama_user ?></label>
												<p class="comment-text"><i class="fa fa-quote-left"></i> <?php echo $dt_comment->isi_command ?> <i class="fa fa-quote-right"></i></p>
												<div class="bottom-comment">
													<div class="comment-date"> <?php echo $m_lib->time_elapsed_string($dt_comment->tbl_command) ?></div>
													<ul class="comment-actions">
														<!-- <li class="reply">Reply</li> -->
													</ul>
												</div>
										</div>
									</div>

								<?php endforeach ?>

								<?php if (empty($this->session->userdata('nomor_telepon'))): ?>
									
									<div class="comment-wrap">
										<div class="photo">
												<div class="avatar" style="background-image: url('https://static.thenounproject.com/png/1506403-200.png')"></div>
										</div>
										<div class="comment-block">
											<form action="<?php echo base_url('front/action_regiter_command'); ?>" method="POST">
												<input type="hidden" name="kode_konten" id="inputKode_topik" class="form-control" value="<?php echo $detail_forum->kode_konten ?>" required="required">
												<div class="form-group">
													<label for="">Nama</label>
													<input type="text" name="nama" class="form-control" id="" placeholder="">
												</div>
												<div class="form-group">
													<label for="">Nomor Telepon</label>
													<input type="text" name="nomor_telepon" class="form-control" id="" placeholder="">
												</div>
												<div class="form-group">
													<label for="">Command</label>
													<textarea name="command" id="" cols="30" rows="3" class="form-control" placeholder="Add comment..."></textarea>
												</div>
												<hr>
												<button type="submit" class="btn pull-right btn-success"><i class="fa fa-comment"></i> Publikasikan</button>
											</form>
										</div>
									</div>

								<?php else: ?>
									
									<div class="comment-wrap">
										<div class="photo">
												<div class="avatar" style="background-image: url('https://www.shareicon.net/download/2015/09/18/103160_man_512x512.png')"></div>
										</div>
										<div class="comment-block">
											<form action="<?php echo base_url('front/action_regiter_command'); ?>" method="POST">
												<label>User Active <div class="text-success"><?php echo $this->session->userdata('nama_user'); ?></div> </label>
												<input type="hidden" name="kode_konten" id="inputKode_topik" class="form-control" value="<?php echo $detail_forum->kode_konten ?>" required="required">
												<div class="form-group">
													<input type="hidden" name="nama" class="form-control" value="<?php echo $this->session->userdata('nama_user'); ?>" placeholder="">
												</div>
												<div class="form-group">
													<input type="hidden" name="nomor_telepon" class="form-control" value="<?php echo $this->session->userdata('nomor_telepon'); ?>" placeholder="">
												</div>
												<div class="form-group">
													<textarea name="command" id="" cols="30" rows="3" class="form-control" placeholder="Komentar..."></textarea>
												</div>
												<hr>
												<a class="btn btn-danger" href="<?php echo base_url('out_status'); ?>" role="button"><i class="fa fa-power-off"></i> Keluar</a>
												<button type="submit" class="btn pull-right btn-success"><i class="fa fa-comment"></i> Reply</button>
											</form>
										</div>
									</div>

								<?php endif ?>

								
						</div>
					</div>
		        </div>
		    </div>

		</div>
	</section>

	<?php //$this->load->view('frontpages/footer'); ?>

	<?php require_once(APPPATH .'views/include/front/front_inc_script.php'); ?>

</body>
</html>