<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once(APPPATH .'views/include/front/front_inc_style.php'); ?>
</head>
<body>
	<?php $this->load->view('frontpages/menu_nav'); ?>

	<section class="slide-show">
		<div class="bg">
			<div class="wthree_banner_info">
				<section class="slider">
					<div class="flexslider">
						<ul class="slides">
							<li>
								<div class="slider-right col-md-6 col-sm-6 hidden-lg hidden-md hidden-sm">
									<img src="<?php echo base_url('asset_front/images/img_slide.png'); ?>" class="img-responsive img-slide-right" alt="Image">
								</div>
								<div class="slider-left col-md-6 col-sm-6">
									<h3>Jadilah Jutawan Bersama Kami</h3>
									<p>Raih Sukses Berternak Puyuh Telur Bersama Kmai, <span class="text-blod">Guyubingmanah.</span> Peluang Usaha Dengan <span class="text-blod">Modal Terjangkau, Hasil Pasti</span> dan <span class="text-blod">Tidak Ribet</span></p>
									<div class="agileits_more">
										<ul>
											<li><a href="#" class=""><img src="<?php echo base_url('asset_front/images/btn_head_1.png') ?>" class="img-responsive" alt="Image"></a></li>
											<li><a href="#" class=""><img src="<?php echo base_url('asset_front/images/btn_head_2.png') ?>" class="img-responsive" alt="Image"></a></li>
										</ul>
									</div>
								</div>
								<div class="slider-right col-md-6 col-sm-6 hidden-xs">
									<img src="<?php echo base_url('asset_front/images/img_slide.png'); ?>" class="img-responsive img-slide-right" alt="Image">
								</div>
							</li>
							<li>
								<div class="slider-right col-md-6 col-sm-6 hidden-lg hidden-md hidden-sm">
									<img src="<?php echo base_url('asset_front/images/img_slide_1.png'); ?>" class="img-responsive img-slide-right" alt="Image">
								</div>
								<div class="slider-left col-md-6 col-sm-6">
									<h3>Usaha Sampingan Tanpa Mengganggu Aktivitas Utama</h3>
									<p>Berternak Puyuh Petelur Bersama <span class="text-blod">Guyubingmanah.</span> Tidak Mengganggu Aktivitas/pekerjaan Utama Anda, Ayo manfaatkan Waktu Luang Anda sekarang juga, <span class="text-blod">Dapatkan Ratusan Ribu Hingga Jutaan Rupiah tiap Minggunya</span></p>
									<div class="agileits_more">
										<ul>
											<li><a href="#" class=""><img src="<?php echo base_url('asset_front/images/btn_head_1.png'); ?>" class="img-responsive" alt="Image"></a></li>
											<li><a href="#" class=""><img src="<?php echo base_url('asset_front/images/btn_head_2.png'); ?>" class="img-responsive" alt="Image"></a></li>
										</ul>
									</div>
								</div>
								<div class="slider-right col-md-6 col-sm-6 hidden-xs">
									<img src="<?php echo base_url('asset_front/images/img_slide_1.png'); ?>" class="img-responsive img-slide-right" alt="Image">
								</div>
								
							</li>
							<li>
								<div class="slider-right col-md-6 col-sm-6 hidden-lg hidden-md hidden-sm">
									<img src="<?php echo base_url('asset_front/images/img_slide_2.png'); ?>" class="img-responsive img-slide-right" alt="Image">
								</div>
								<div class="slider-left col-md-6 col-sm-6">
									<h3>Ternak Puyuh Tidak Memerlukan Lahan Luas</h3>
									<p>Anda bisa memanfaatkan halaman belakang rumah anda untuk dijadikan pendapatan tambahan untuk keluarga anda</p>
									<div class="agileits_more">
										<ul>
											<li><a href="#" class=""><img src="<?php echo base_url('asset_front/images/btn_head_1.png'); ?>" class="img-responsive" alt="Image"></a></li>
											<li><a href="#" class=""><img src="<?php echo base_url('asset_front/images/btn_head_2.png'); ?>" class="img-responsive" alt="Image"></a></li>
										</ul>
									</div>
								</div>
								<div class="slider-right col-md-6 col-sm-6 hidden-xs">
									<img src="<?php echo base_url('asset_front/images/img_slide_2.png'); ?>" class="img-responsive img-slide-right" alt="Image">
								</div>
							</li>
						</ul>
					</div>
				</section>
			</div>
			<div class="img-right-petani">
				<img src="<?php echo base_url('asset_front/images/body_right_1.png'); ?>" class="img-responsive" alt="Image">
			</div>

			<div class="profil">
				<div class="img-left-mascot">
					<img src="<?php echo base_url('asset_front/images/mascot_puyuh_1.png'); ?>" class="img-responsive hidden-xs" alt="Image">
					<img src="<?php echo base_url('asset_front/images/mascot_puyuh_sm_1.png'); ?>" class="img-responsive hidden-lg hidden-md hidden-sm" alt="Image">
				</div>

				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<div class="text-profil">
						<div class="desc-profil">
							<h3>Sekilas Tentang Guyubingmanah</h3>
							<p>
								<strong>GUYUBINGMANAH</strong> adalah Paguyuban Dan Kemitraan Puyuh Petelur terbesar di provinsi Jawa Tengah dan D.I. Yogyakarta yang telah berdiri sejak tahun 2000 dengan pimpinan pusat berada di Brosot, Kulonprogo, Yogyakarta.
								<br><br>
								Awal berdirinya paguyuban karena peternak puyuh masih terkendala ketersediaan bibit puyuh yang berkualitas, pendistribusian pakan yang belum merata serta pemasaran telur yang masih lokal sehingga harga telur belum stabil.
							</p>
							<div class="bottom-next">
								<a href="<?php echo base_url('profil'); ?>" title=""><img src="<?php echo base_url('asset_front/images/btn_selengkapnya.png'); ?>" class="img-responsive" alt="Image"></a>
							</div>
						</div>
						<div class="desc-keuntungan">
							<h3>Mengapa Anda Harus Bergabung Dengan Guyubingmanah?</h3>
							<ul>
								<li>Keuntungan Puyuh Petelur Menjanjikan.</li>
								<li>Paguyuban Puyuh Petelur Terbesar Di Jogja - Jawa Tengah.</li>
								<li>Berpengalaman Lebih Dari 15 Tahun.</li>
								<li>Telah Mencetak Ratusan Peternak Puyuh Sukses.</li>
								<li>Bimbingan Aktif Kepada Anggota Kemitraan.</li>
							</ul>
							<div class="bottom-next-keuntungan">
								<a href="<?php echo base_url('daftar_guyu'); ?>" title=""><img src="<?php echo base_url('asset_front/images/btn_head_2.png'); ?>" class="img-responsive left-btn sm-btn-left" alt="Image"></a>
								<a href="https://api.whatsapp.com/send?phone=6281328818656&text=Saya%20Mau%20Konsultasi" title=""><img src="<?php echo base_url('asset_front/images/btn_head_1.png'); ?>" class="img-responsive " alt="Image"></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<!-- <div class="img-right-bg">
				<img src="<?php echo base_url('asset_front/images/body_right_2.png'); ?>" class="img-responsive" alt="Image">
			</div> -->
			
			<div class="alasan-ternak">
				
				<div class="container-fluid">
					<div class="title-alasan">
						<h4>Alasan Anda Harus Berternak Puyuh</h4>
					</div>
					<div class="col-xs-12 col-sm-4 col-lg-4 hidden-lg hidden-md hidden-sm">
						<div class="icon-peternak">
							<img src="<?php echo base_url('asset_front/images/icon_center.png') ?>" class="img-responsive" alt="Image">
							<div class="bottom-peternak">
								<a href="" title=""><img src="<?php echo base_url('asset_front/images/btn_gabung.png') ?>" class="img-responsive" alt="Image"></a>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
						<div class="col-lg-12">
							<div class="position-left">
								<div class="icon-left">
									<img src="<?php echo base_url('asset_front/images/icon_1.png') ?>" class="img-responsive" alt="Image">
								</div>
								<div class="desc-left">
									<h3>01</h3>
									<h4>Tabungan & Investasi</h4>
									<p>Puyuh mulai bertelur saat berumur 38 hari. masa produktif sampai 14 bulan.</p>
								</div>
							</div>
						</div>	
						<div class="col-lg-12">
							<div class="position-left">
								<div class="icon-left">
									<img src="<?php echo base_url('asset_front/images/icon_2.png') ?>" class="img-responsive" alt="Image">
								</div>
								<div class="desc-left">
									<h3>02</h3>
									<h4>Minimum Resiko</h4>
									<p>Puyuh tahan perubahan cuaca & minim resiko penyakit.</p>
								</div>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="position-left">
								<div class="icon-left">
									<img src="<?php echo base_url('asset_front/images/icon_3.png') ?>" class="img-responsive" alt="Image">
								</div>
								<div class="desc-left">
									<h3>03</h3>
									<h4>Modal Terjangkau</h4>
									<p>Tidak diperlukan modal besar dan lahan yang luas.</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4 hidden-md hidden-sm hidden-xs">
						<div class="icon-peternak">
							<img src="<?php echo base_url('asset_front/images/icon_center.png') ?>" class="img-responsive" alt="Image">
							<div class="bottom-peternak">
								<a href="<?php echo base_url('daftar_guyu'); ?>" title=""><img src="<?php echo base_url('asset_front/images/btn_gabung.png') ?>" class="img-responsive" alt="Image"></a>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
						<div class="col-lg-12">
							<div class="position-right">
								<div class="icon-right">
									<img src="<?php echo base_url('asset_front/images/icon_4.png') ?>" class="img-responsive" alt="Image">
								</div>
								<div class="desc-right">
									<h3>04</h3>
									<h4>Permintaan Telur Puyuh Tinggi</h4>
									<p>Konsumsi telur puyuh di wilayah Jawa Tengah & D.I.Yogyakarta tinggi</p>
								</div>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="position-right">
								<div class="icon-right">
									<img src="<?php echo base_url('asset_front/images/icon_5.png') ?>" class="img-responsive" alt="Image">
								</div>
								<div class="desc-right">
									<h3>05</h3>
									<h4>Afkir Puyuh Banyak Dicari</h4>
									<p>Daging puyuh dimintai pecinta kuliner, permintaan pasar tinggi</p>
								</div>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="position-right">
								<div class="icon-right">
									<img src="<?php echo base_url('asset_front/images/icon_6.png') ?>" class="img-responsive" alt="Image">
								</div>
								<div class="desc-right">
									<h3>06</h3>
									<h4>Kotoran Bernilai Ekonomis</h4>
									<p>Alternatif pakan ikan dan pupuk kandang (crude protein tinggi 28%-Riset IPB)</p>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</section>

	<section class="maps-wilayah">
		<div class="header-title">
			<h3>Ketua Kelompok & Jumlah Anggota Paguyuban Peternak Puyuh Petelur Kami Di WIlayah Jateng - DIY</h3>
		</div>
		<div class="container top-18">
			<div class="hidden-xs col-sm-12 hidden-md hidden-lg">
				<div class="maps-jawatengah">
					<img src="<?php echo base_url('asset_front/images/maps_jawa.png'); ?>" class="img-responsive" alt="Image">
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
				<div class="frame-lev1">
					<div class="wilayah-koordinator1">
						<img src="<?php echo base_url('asset_front/images/avatar_tarto.png'); ?>" class="img-responsive" alt="Image">
					</div>
					<div class="kontak-koordinator">
						<div class="arrow-kordinator1">
							<img src="<?php echo base_url('asset_front/images/asset_lg_right.png'); ?>" class="img-responsive" alt="Image">
						</div>
						<div class="data-koordinator">
							<h3>Sutarto</h3>
							<h3>081328818656</h3>
							<p>Wilayah Pusat Seluruh Wilayah Jateng - DIY</p>
						</div>
					</div>
					<div class="data-real">
						<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
							<div class="text-peternak">
								<div class="img-icon">
									<img src="<?php echo base_url('asset_front/images/icon_sm1.png'); ?>" class="img-responsive" alt="Image">
									<span class="ket-populasi">Plasma</span>
								</div>
								<?php if ($data_populasi_005->JumPlas != 0): ?>
									<span class="count-populasi"><?php echo $data_populasi_005->JumPlas; ?></span>
								<?php else: ?>
									<span class="count-populasi"><span class="label label-info"><i class="fa fa-refresh"></i> Update data...</span></span>
								<?php endif ?>
								
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
							<div class="text-populasi">
								<div class="img-icon">
									<img src="<?php echo base_url('asset_front/images/icon_sm2.png'); ?>" class="img-responsive" alt="Image">
									<span class="ket-populasi">Populasi</span>
								</div>
								<?php if (number_format($data_populasi_005->Populasi ,0,'','.') != 0): ?>
									<span class="count-populasi"><?php echo number_format($data_populasi_005->Populasi ,0,'','.') ?></span>
								<?php else: ?>
									<span class="count-populasi"><span class="label label-info"><i class="fa fa-refresh"></i> Update data...</span></span>
								<?php endif ?>
								
							</div>
						</div>
					</div>
				</div>

				<div class="frame-lev2">
					<div class="wilayah-koordinator2">
						<img src="<?php echo base_url('asset_front/images/avatar_sigit.png'); ?>" class="img-responsive" alt="Image">
					</div>
					<div class="kontak-koordinator">
						<div class="arrow-kordinator2">
							<img src="<?php echo base_url('asset_front/images/asset_lg_right.png'); ?>" class="img-responsive" alt="Image">
						</div>
						<div class="data-koordinator">
							<h3>Sigit Purnomo</h3>
							<h3>081804382439</h3>
							<p>Wilayah Gombong Kebumen, Banyumas,Cilacap, Wonosobo, Banjarnegara, Purbalingga</p>
						</div>
					</div>
					<div class="data-real">
						<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
							<div class="text-peternak">
								<div class="img-icon">
									<img src="<?php echo base_url('asset_front/images/icon_sm1.png'); ?>" class="img-responsive" alt="Image">
									<span class="ket-populasi">Plasma</span>
								</div>
								<?php if ($data_populasi_008->JumPlas != 0): ?>
									<span class="count-populasi"><?php echo $data_populasi_008->JumPlas; ?></span>
								<?php else: ?>
									<span class="count-populasi"><span class="label label-info"><i class="fa fa-refresh"></i> Update data...</span></span>
								<?php endif ?>
								
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
							<div class="text-populasi">
								<div class="img-icon">
									<img src="<?php echo base_url('asset_front/images/icon_sm2.png'); ?>" class="img-responsive" alt="Image">
									<span class="ket-populasi">Populasi</span>
								</div>
								<?php if (number_format($data_populasi_008->Populasi ,0,'','.') != 0): ?>
									<span class="count-populasi"><?php echo number_format($data_populasi_008->Populasi ,0,'','.') ?></span>
								<?php else: ?>
									<span class="count-populasi"><span class="label label-info"><i class="fa fa-refresh"></i> Update data...</span></span>
								<?php endif ?>
								
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="hidden-xs hidden-sm col-md-6 col-lg-6">
				<div class="maps-jawatengah">
					<img src="<?php echo base_url('asset_front/images/maps_jawa.png'); ?>" class="img-responsive" alt="Image">
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
				<div class="frame-lev3">
					<div class="wilayah-koordinator3">
						<img src="<?php echo base_url('asset_front/images/avatar_nanang.png'); ?>" class="img-responsive" alt="Image">
					</div>
					<div class="kontak-koordinator">
						<div class="arrow-kordinator3">
							<img src="<?php echo base_url('asset_front/images/asset_lg_left.png') ?>" class="img-responsive" alt="Image">
						</div>
						<div class="data-koordinator">
							<h3>Nanang S</h3>
							<h3>087838288820</h3>
							<p>Wilayah Brosot Bantul, Kulonprogo, Purworejo, Kutoarjo</p>
						</div>
					</div>
					<div class="data-real">
						<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
							<div class="text-peternak">
								<div class="img-icon">
									<img src="<?php echo base_url('asset_front/images/icon_sm1.png'); ?>" class="img-responsive" alt="Image">
									<span class="ket-populasi">Plasma</span>
								</div>
								<?php if ($data_populasi_001->JumPlas != 0): ?>
									<span class="count-populasi"><?php echo $data_populasi_001->JumPlas; ?></span>
								<?php else: ?>
									<span class="count-populasi"><span class="label label-info"><i class="fa fa-refresh"></i> Update data...</span></span>
								<?php endif ?>
								
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
							<div class="text-populasi">
								<div class="img-icon">
									<img src="<?php echo base_url('asset_front/images/icon_sm2.png'); ?>" class="img-responsive" alt="Image">
									<span class="ket-populasi">Populasi</span>
								</div>
								<?php if (number_format($data_populasi_001->Populasi ,0,'','.') != 0): ?>
									<span class="count-populasi"><?php echo number_format($data_populasi_001->Populasi ,0,'','.') ?></span>
								<?php else: ?>
									<span class="count-populasi"><span class="label label-info"><i class="fa fa-refresh"></i> Update data...</span></span>
								<?php endif ?>
								
							</div>
						</div>
					</div>
				</div>

				<div class="frame-lev4">
					<div class="wilayah-koordinator4">
						<img src="<?php echo base_url('asset_front/images/avatar_aziz.png') ?>" class="img-responsive" alt="Image">
					</div>
					<div class="kontak-koordinator">
						<div class="arrow-kordinator4">
							<img src="<?php echo base_url('asset_front/images/asset_lg_left.png') ?>" class="img-responsive" alt="Image">
						</div>
						<div class="data-koordinator">
							<h3>Aziz Risdianto</h3>
							<h3>085228378830</h3>
							<p>Wilayah Kalasan Sleman, Magelang, Klaten, Gunungkidul, Wonogiri & Solo</p>
						</div>
					</div>
					<div class="data-real">
						<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
							<div class="text-peternak">
								<div class="img-icon">
									<img src="<?php echo base_url('asset_front/images/icon_sm1.png'); ?>" class="img-responsive" alt="Image">
									<span class="ket-populasi">Plasma</span>
								</div>
								<?php if ($data_populasi_003->JumPlas != 0): ?>
									<span class="count-populasi"><?php echo $data_populasi_003->JumPlas; ?></span>
								<?php else: ?>
									<span class="count-populasi"><span class="label label-info"><i class="fa fa-refresh"></i> Update data...</span></span>
								<?php endif ?>
								
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
							<div class="text-populasi">
								<div class="img-icon">
									<img src="<?php echo base_url('asset_front/images/icon_sm2.png'); ?>" class="img-responsive" alt="Image">
									<span class="ket-populasi">Populasi</span>
								</div>
								<?php if (number_format($data_populasi_003->Populasi ,0,'','.') != 0): ?>
									<span class="count-populasi"><?php echo number_format($data_populasi_003->Populasi ,0,'','.') ?></span>
								<?php else: ?>
									<span class="count-populasi"><span class="label label-info"><i class="fa fa-refresh"></i> Update data...</span></span>
								<?php endif ?>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="tahapan-ternak">
		<div class="container-fluid">
			<div class="title-tahapan-md hidden-lg">
				<h2>Tahapan-tahapan Beternak Bersama Guyubingmanah</h2>
			</div>
			<div class="hidden-xs hidden-sm hidden-md col-lg-4">
				<div class="title-ternak">
					<h3>Tahapan-tahapan Beternak Bersama Guyubingmanah</h3>
				</div>
				<div class="foto-peternak">
					<img src="<?php echo base_url('asset_front/images/img_body_left.png') ?>" class="img-responsive" alt="Image">
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
				
				<div class="row">
				    <div class="col-xs-12 col-lg-4 col-sm-4 col-md-4 align-self-start">
				      	<div class="maskot-tahap-1">
				      		<p class="number-maskot-1">1</p>
				      		<div class="arrow-tahap-1">
				      			<img src="<?php echo base_url('asset_front/images/asset_arraw_1.png') ?>" class="img-responsive" alt="Image">
				      		</div>
				      		<div class="img-maskot-1">
				      			<img src="<?php echo base_url('asset_front/images/mascot_tahap1.png') ?>" class="img-responsive img-maskot" alt="Image">
				      		</div>
				      		<div class="img-label-maskot-1">
				      			<img src="<?php echo base_url('asset_front/images/asset_tahapan.png') ?>" class="img-responsive " alt="Image">
				      			<p>Konsultasi</p>
				      		</div>
				      		<div class="box-keterangan">
				      			<p>
				      				Silahkan hubungi ketua 
									kelompok daerah Anda 
									untuk mendapatkan 
									informasi detail tentang 
									kemitraan puyuh petelur
									Guyubingmanah.
				      			</p>
				      			<a class="btn btn-success btn-tahapan-custom" href="https://api.whatsapp.com/send?phone=6281328818656&text=Saya%20Mau%20Konsultasi" role="button">Konsultasi Sekarang</a>
				      		</div>
				      	</div>
				    </div>
				    <div class="col-xs-12 col-lg-4 col-sm-4 col-md-4 align-self-center">
				    	<div class="arrow-tahap-2">
				    		<p class="number-maskot-2">2</p>
			      			<img src="<?php echo base_url('asset_front/images/asset_arraw_1.png'); ?>" class="img-responsive" alt="Image">
			      		</div>
				      	<div class="maskot-tahap-2">
				      		<div class="maskot-tahap-2">
					      		<div class="img-maskot-2">
					      			<img src="<?php echo base_url('asset_front/images/mascot_tahap2.png'); ?>" class="img-responsive img-maskot" alt="Image">
					      		</div>
					      		<div class="img-label-maskot-2">
					      			<img src="<?php echo base_url('asset_front/images/asset_tahapan.png'); ?>" class="img-responsive " alt="Image">
					      			<p>Persiapan</p>
					      		</div>
					      		<div class="box-keterangan">
					      			<p>
					      				Pendamping kelompok 
										akan memberikan 
										gambaran kebutuhan 
										ternak (kandang, 
										perlengkapan minum, 
										dsb) demi keberhasilan 
										ternak puyuh Anda.
					      			</p>
					      			<a class="btn btn-success btn-tahapan-custom" href="https://api.whatsapp.com/send?phone=6281328818656&text=Saya%20Mau%20Konsultasi" role="button">Konsultasi Sekarang</a>
					      		</div>
					      	</div>
				      	</div>
				    </div>
				    <div class="col-xs-12 col-lg-4 col-sm-4 col-md-4 lign-self-end">
				      	<div class="maskot-tahap-3">
				      		<p class="number-maskot-3">3</p>
				      		<div class="maskot-tahap-3">
					      		<div class="img-maskot-3">
					      			<img src="<?php echo base_url('asset_front/images/mascot_tahap3.png') ?>" class="img-responsive img-maskot" alt="Image">
					      		</div>
					      		<div class="img-label-maskot-3">
					      			<img src="<?php echo base_url('asset_front/images/asset_tahapan.png') ?>" class="img-responsive " alt="Image">
					      			<p>Berternak</p>
					      		</div>
					      		<div class="box-keterangan">
					      			<p>
					      				DOQ dan pakan akan 
										kami kirim ke kandang 
										Anda sesuai dengan 
										jadwal yang telah 
										disepakati.
					      			</p>
					      			<a class="btn btn-success btn-tahapan-custom" href="https://api.whatsapp.com/send?phone=6281328818656&text=Saya%20Mau%20Konsultasi" role="button">Konsultasi Sekarang</a>
					      		</div>
					      	</div>
				      	</div>
				    </div>
				</div>
		</div>
	</section>

	<section class="testimoni">
		<div class="container">
			<div class="title-testimoni">
				<h2>Apa Kata Peternak Kami</h2>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div id="carousel-id" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#carousel-id" data-slide-to="0" class=""></li>
						<li data-target="#carousel-id" data-slide-to="1" class="active"></li>
					</ol>
					<div class="carousel-inner">
						<div class="item active">
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<div class="box-testimoni">
									<div class="isi-testimoni">
										<p>
											Saya bergabung menjadi sejak tahun 2000, dengan populasi 500 ekor, selanjutanya 3000 ekor karena keterbatasan lahan.
											Dengan menjadi anggota, harga telur stabil, perawatan puyuh terbilang mudah sehingga tidak mengganggu aktifitas utama di sawah.
										</p>
										<p class="nama-testimoni">
											SUGIMAN (40 th) Petani Cangkringan, Sleman
										</p>
									</div>
									<img src="<?php echo base_url('asset_front/images/ava_testi_1.png'); ?>" class="img-responsive" alt="Image">
								</div>
							</div>
							<div class="hidden-xs col-sm-4 col-md-4 col-lg-4">
								<div class="box-testimoni">
									<div class="isi-testimoni">
										<p>
											Info awal dari teman yang telah sukses, tahun 2016 memutuskan bergabung dengan populasi awal 5.000 ekor.
											Benefit menjanjikan, tidak mengganggu aktifitas utama, saya tidak perlu khawatir masalah telur karena sudah diambil.
										</p>
										<p class="nama-testimoni">
											M. DANU WARDANA Usaha Cuci Mobil Hargomulyo, Sleman
										</p>
									</div>
									<img src="<?php echo base_url('asset_front/images/ava_testi_2.png'); ?>" class="img-responsive" alt="Image">
								</div>
							</div>
							<div class="hidden-xs col-sm-4 col-md-4 col-lg-4">
								<div class="box-testimoni">
									<div class="isi-testimoni">
										<p>
											Bergabung dengan paguyuban ditahun 2009 dengan populasi 2.800 ekor.
											<br>
											Dengan beternak puyuh saya dapat menghemat pakan  ikan karena kotoran puyuh dapat dimanfaatkan menjadi pakan ikan, sehingga saya untung dobel, panen telur dan juga panen ikan.
										</p>
										<p class="nama-testimoni">
											NUHADI (49th) Petani Ikan Cangkringan, Sleman.
										</p>
									</div>
									<img src="<?php echo base_url('asset_front/images/ava_testi_3.png'); ?>" class="img-responsive" alt="Image">
								</div>
							</div>
						</div>
						<div class="item">
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<div class="box-testimoni">
									<div class="isi-testimoni">
										<p>
											Sejak tahun 2012 bergabung, karena teman saya yang juga Mantan TKI lebih dahulu bergabung dan sukses beternak puyuh.
											Saya yang awam didunia peternakan, didampingi paguyuban mulai dari pembuatan kandang, pemeliharaan hingga pengelolaan limbah. Awal beternak 2000 ekor, bulan ke empat menjadi 4000 ekor, hingga sekarang 10.000 ekor.
										</p>
										<p class="nama-testimoni">
											WARDI KUSNANTO (31 th) Mantan TKI Malaysia Temon, Kulonprogo
										</p>
									</div>
									<img src="<?php echo base_url('asset_front/images/ava_testi_4.png'); ?>" class="img-responsive" alt="Image">
								</div>
							</div>
							<div class="hidden-xs col-sm-4 col-md-4 col-lg-4">
								<div class="box-testimoni">
									<div class="isi-testimoni">
										<p>
											Awal tahun 2014 saya mulai bergabung menjadi peternak Puyuh.
											<br>
											Lahan saya hanya 7x10 meter, mampu menampung 4.000 ekor, jadi jangan khawatir jika teman-teman tidak mempunyai lahan luas.
											<br>
											Selama Bergabung, jika ada masalah apapun tentang puyuh, tinggal telp langsung datang pembimbing peternak dari paguyuban.
										</p>
										<p class="nama-testimoni">
											ANDI (40 th)
											Tukang Batu
											Ngemplak, Sleman
										</p>
									</div>
									<img src="<?php echo base_url('asset_front/images/ava_testi_5.png'); ?>" class="img-responsive" alt="Image">
								</div>
							</div>
							<div class="hidden-xs col-sm-4 col-md-4 col-lg-4">
								<div class="box-testimoni">
									<div class="isi-testimoni">
										<p>
											Sebelumnya Saya beternak Ayam & Sapi namun semua Gagal.
											Di tahun 2015 saya bergabung, dengan populasi awal 2.200 ekor , tahun ketiga menjadi 14.200 ekor
											Puyuh tidak membutuhkan lahan luas, modal kecil, tetapi lebih untung Servis Paguyuban luar biasa, Bibit berkualitas, pakan diantar sampe kandang, dan telur diambil.
										</p>
										<p class="nama-testimoni">
											IHSANUDIN Pegawai Wates, Kulonprogo
										</p>
									</div>
									<img src="<?php echo base_url('asset_front/images/ava_testi_6.png'); ?>" class="img-responsive" alt="Image">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php $this->load->view('frontpages/footer'); ?>

	<?php require_once(APPPATH .'views/include/front/front_inc_script.php'); ?>

</body>
</html>