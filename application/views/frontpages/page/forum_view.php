<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once(APPPATH .'views/include/front/front_inc_style.php'); ?>
</head>
<body>
	<?php $this->load->view('frontpages/menu_nav'); ?>
	
	<section class="konten-forum">
		<div class="container-fluid">
			
			<div class="table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Forum</th>
							<th>Topik</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($data_kategori as $dt_kategori): ?>
						<tr>
							
							<td>
								<a href="<?php echo base_url('kategori/'.$dt_kategori->kategori); ?>" title="<?php echo $dt_kategori->kategori ?>">
									<?php echo strtoupper($dt_kategori->kategori) ?>
									<br>
									<?php if ($dt_kategori->kategori == "persiapan"): ?>
										<small>Persiapan consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</small>
									<?php elseif($dt_kategori->kategori == "starter_puyuh"): ?>
										<small>Starter Puyuh consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</small>
									<?php elseif($dt_kategori->kategori == "doq"): ?>
										<small>DOQ (Day Of Quail) consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</small>
									<?php elseif($dt_kategori->kategori == "pemeliharaan"): ?>
										<small>Pemeliharaan consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</small>
									<?php elseif($dt_kategori->kategori == "panen"): ?>
										<small>Panen consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</small>
									<?php elseif($dt_kategori->kategori == "penyakit"): ?>
										<small>Penyakit consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</small>
									<?php else: ?>
										<small>New Topik..</small>
									<?php endif ?>
								</a>
							</td>
							
							<td><?php echo($dt_kategori->total_data) ?></td>
						</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>

		</div>
	</section>

	<?php //$this->load->view('frontpages/footer'); ?>

	<?php require_once(APPPATH .'views/include/front/front_inc_script.php'); ?>

</body>
</html>