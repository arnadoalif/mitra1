<section class="kontak-koordinator-profil">
	<div class="img-kontak">
		<img src="<?php echo base_url('asset_front/images/asset_profile.png'); ?>" class="img-responsive" alt="Image">
		<div class="profil-kontak">
			<div class="container">
				<div class="row">
					<div class="hidden-xs col-sm-12 col-md-4 col-lg-4">
						<div class="img-maskot-kontak">
							<div class="lingkaran">
								<p>Ingin Bergabung ?</p>
							</div>
							<img src="<?php echo base_url('asset_front/images/mascot_profile.png'); ?>" class="img-responsive" alt="Image">
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
							<div class="daftar-kontak-profil">
								<div class="title-wilayah">
									<p>
										<span class="bold-title-wilayah">Pimpinan Pusat</span><br>
										Pulo, Brosot, Galur, Kulon Progo
										(0274) 2850064
									</p>
								</div>
								<br>
								<div class="nama-koordinator">
									<p>
										<strong> Sutarto </strong> <br>
										<a href="https://api.whatsapp.com/send?phone=6281328818656&text=Saya%20Mau%20Konsultasi">081328818656</a>
									</p>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
							<div class="daftar-kontak-profil">
								<div class="title-wilayah">
									<p>
										<span class="bold-title-wilayah">Wilayah Kulonprogo </span><br>
										Area : Bantul, Kulonprogo, Purworejo, Kutoarjo
									</p>
								</div>
								<br>
								<div class="nama-koordinator">
									<p>
										<strong>Nanang S</strong> <br>
										<a href="https://api.whatsapp.com/send?phone=6287838288820&text=Saya%20Mau%20Konsultasi">087838288820</a>
									</p>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
							<div class="daftar-kontak-profil">
								<div class="title-wilayah">
									<p>
										<span class="bold-title-wilayah">Wilayah Kebumen</span><br>
										Area : Kebumen, Banyumas, Cilacap, Wonosobo, Banjarnegara, Purbalingga
									</p>
								</div>
								<br>
								<div class="nama-koordinator">
									<p>
										<strong> Sigit Purnomo </strong> <br>
										<a href="https://api.whatsapp.com/send?phone=6281804382439&text=Saya%20Mau%20Konsultasi">081804382439</a>
									</p>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
							<div class="daftar-kontak-profil">
								<div class="title-wilayah">
									<p>
										<span class="bold-title-wilayah">Wilayah Sleman </span><br>
										Area : Sleman, Magelang, Klaten, Gunungkidul, Wonogiri dan Solo
									</p>
								</div>
								<br>
								<div class="nama-koordinator">
									<p>
										<strong> Aziz Risdianto </strong> <br>
										<a href="https://api.whatsapp.com/send?phone=6285228378830&text=Saya%20Mau%20Konsultasi">085228378830</a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>