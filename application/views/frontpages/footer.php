<div class="footer">
		<div class="col-lg-3 col-md-3 hidden-sm footer-left-agileits">
			<img src="<?php echo base_url('asset_front/images/icon_footer.png'); ?>" class="img-responsive" alt="Image">
		</div>
		<div class="col-lg-3 col-md-3 footer-left-agileinfo">
			<h3>Tentang Kami</h3>
			<ul class="footer-tentang">
				<li>Kantor : Pulo, Brosot, Galur, Kulon Progo</li>
				<li>Telp : 0274-2850064</li>
				<li>Email : <a href="mailto:guyubingmanah@gmail.com?subject=feedback" >guyubingmanah@gmail.com</a></li>
				<li>Hotline : 081328818656</li>
			</ul>
		</div>
		<div class="col-lg-3 col-md-3 footer-left-agileinfo">
			<h3>Service Kami</h3>
			<ul class="footer-tentang">
				<li><a href="<?php echo base_url(); ?>">Beranda</a></li>
				<li><a href="<?php echo base_url('profil'); ?>">Profil</a></li>
				<li><a href="<?php echo base_url('analisa_usaha'); ?>">Analisa Usaha</a></li>
				<!-- <li><a href="">Blog</a></li> -->
				<li><a href="<?php echo base_url('daftar_guyu'); ?>">Daftar Guyubingmanah</a></li>
			</ul>
		</div>
		<div class="col-lg-3 col-md-3 footer-left-w3-agileits">
			<h3>Follow Kami:</h3>
				<ul class="social-icons">
					<li><a href="#" class="icon icon-border facebook"></a></li>
					<li><a href="#" class="icon icon-border instagram"></a></li>
					<li><a href="https://api.whatsapp.com/send?phone=6281328818656&text=Saya%20Mau%20Konsultasi" class="icon icon-border whatsapp"></a></li>
				</ul>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="copyright-w3-agile">
		<div class="container">
			<p>© 2018 Web Developer | Guyubingmanah</p>
			<div class="clearfix"> </div>
		</div>
	</div>