<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'front/profil';
$route['404_override'] = 'front/profil';
$route['translate_uri_dashes'] = FALSE;

$route['admin'] = 'admin';
$route['admin/data_plasma'] = 'admin/data_plasma';


$route['super'] = 'super';
$route['super/data_plasma'] = 'super/super_data_plasma';
$route['super/id_plasma'] = 'super/get_data_plasma';
$route['super/analisa'] = 'super/analisa_usaha';
$route['super/create_forum'] = 'super/create_forum_view';
$route['super/open/(:any)'] = 'super/view_list_kontent/$1';
$route['super/edit/(:any)'] = 'super/edit_data_konten/$1';
$route['super/delete/(:any)'] = 'super/action_delete_konten/$1';

$route['home'] = 'front';
$route['profil'] = 'front/profil';
$route['analisa_usaha'] = 'front/analisa_usaha';
$route['daftar_guyu'] = 'front/daftar_guyu';
$route['forum'] = 'front/forum_view';
$route['kategori/(:any)'] = 'front/list_post/$1';
$route['konten/(:any)'] = 'front/konten_view/$1';

$route['out_status'] = 'front/out_command';


$route['login'] = 'login';
$route['logout'] = 'login/logout';
