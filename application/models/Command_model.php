<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Command_model extends CI_Model {

	
	public function check_user_command($table_name, $kode_konten, $nama, $nomor_telepon) {
		$valid = $this->db->where('kode_konten', $kode_konten)->where('nama_user', $nama)->where('nomor_telepon', $nomor_telepon);
		$valid = $this->db->get($table_name);
		if ($valid->num_rows() > 0) {
			return $valid;
		} else {
			return false;
		}
	}

	public function get_data_command_by_kode_kontent($table_name, $kode_konten) {
		$this->db->where('kode_konten', $kode_konten);
		$this->db->order_by('tgl_command', 'asc');
		return $this->db->get($table_name);
	}

	public function count_data_command_by_kode_kontent($table_name, $kode_konten) {
		$this->db->where('kode_konten', $kode_konten);
		return $this->db->get($table_name)->num_rows();
	}

	public function insert_data_command($table_name, $data) {
		$this->db->insert($table_name, $data);
	}

	public function delete_data_command($table_name, $kode_key, $field_name) {
		$valid = $this->db->where($field_name, $kode_key);
		$valid = $this->db->get($table_name, 1);

		if ($valid->num_rows() > 0) {
			$this->db->where($field_name, $kode_key);
			$this->db->delete($table_name);
			return true;
		} else {
			return false;
		}
	}

}

/* End of file Command_model.php */
/* Location: ./application/models/Command_model.php */