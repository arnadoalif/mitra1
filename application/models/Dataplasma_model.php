<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dataplasma_model extends CI_Model {

	public function detail_plasma($periode, $kode_zona) {
		$sql = "
				Declare @Per Varchar(4)='$periode'
				Declare @KdZona Varchar(4)='$kode_zona'

				Select Z.Kode_Plas,B.Namaplas,SUM(Z.Populasi) As Populasi, SUM(Z.Surplus) As Surplus, SUM(Z.Tabungan) As Tabungan
				From(Select Kode_Plas, Populasi, 0 As Surplus, 0 As Tabungan
						from LINK_POP.Mitra18.dbo.FeedIntake where Periode=@Per and Kode_Zona=@KdZona
					Union All
					SELECT A.kode_plas, 0 As Populasi, A.jumin As Surplus, 0 As Tabungan
						FROM LINK_POP.Mitra18.dbo.AcPiutPlasMit1 As A Inner Join LINK_POP.Mitra18.dbo.REFPLASMA As B On A.kode_plas=B.Kode_plas Inner Join LINK_POP.Mitra18.dbo.REFSEKSI As C On B.Kode_seksi=C.Kode_seksi
						WHERE A.Periode=@Per and C.Kode_zona=@KdZona and A.kodebrg='PAY' and Substring(A.RefNo,1,4)='CSQ4' 
					Union All
					SELECT A.kode_plas, 0 As Populasi, 0 As Surplus, Ceiling(SUM(JumOut)) As Tabungan
						FROM LINK_POP.Mitra18.dbo.AcSavePlasMit1 As A Inner Join LINK_POP.Mitra18.dbo.REFPLASMA As B On A.kode_plas=B.Kode_plas Inner Join LINK_POP.Mitra18.dbo.REFSEKSI As C On B.Kode_seksi=C.Kode_seksi
						WHERE A.Periode=@Per and C.Kode_zona=@KdZona Group By A.kode_plas
				) As Z Inner Join LINK_POP.Mitra18.dbo.REFPLASMA As B On Z.Kode_Plas=B.Kode_plas 
				Group By Z.Kode_Plas,B.Namaplas
				Having SUM(Z.Populasi)<>0 
			";
		return $this->db->query($sql);
	}

	public function total_populasi($periode, $kode_zona) {
		$sql = "
			Declare @Per Varchar(4)='$periode'
			Declare @KdZona Varchar(4)='$kode_zona'

			Select Count(*) As JumPlas, SUM(Populasi) As Populasi, SUM(Surplus) As Surplus, SUM(Tabungan) As Tabungan, Case When Count(*)<>0 Then Ceiling((SUM(Surplus)+SUM(Tabungan))/Count(*)) Else 0 End As RataSurplus
				From(Select Z.Kode_Plas, SUM(Z.Populasi) As Populasi, SUM(Z.Surplus) As Surplus, SUM(Z.Tabungan) As Tabungan
					From(Select Kode_Plas, Populasi, 0 As Surplus, 0 As Tabungan
							from LINK_POP.Mitra18.dbo.FeedIntake where Periode=@Per and Kode_Zona=@KdZona
						Union All
						SELECT A.kode_plas, 0 As Populasi, A.jumin As Surplus, 0 As Tabungan
							FROM LINK_POP.Mitra18.dbo.AcPiutPlasMit1 As A Inner Join LINK_POP.Mitra18.dbo.REFPLASMA As B On A.kode_plas=B.Kode_plas Inner Join LINK_POP.Mitra18.dbo.REFSEKSI As C On B.Kode_seksi=C.Kode_seksi
							WHERE A.Periode=@Per and C.Kode_zona=@KdZona and A.kodebrg='PAY' and Substring(A.RefNo,1,4)='CSQ4' 
						Union All
						SELECT A.kode_plas, 0 As Populasi, 0 As Surplus, Ceiling(SUM(JumOut)) As Tabungan
							FROM LINK_POP.Mitra18.dbo.AcSavePlasMit1 As A Inner Join LINK_POP.Mitra18.dbo.REFPLASMA As B On A.kode_plas=B.Kode_plas Inner Join LINK_POP.Mitra18.dbo.REFSEKSI As C On B.Kode_seksi=C.Kode_seksi
							WHERE A.Periode=@Per and C.Kode_zona=@KdZona Group By A.kode_plas
					) As Z Inner Join LINK_POP.Mitra18.dbo.REFPLASMA As B On Z.Kode_Plas=B.Kode_plas  Group By Z.Kode_Plas
				) As X 
			Where Populasi<>0 
		 ";
		return $this->db->query($sql);
	}

}

/* End of file Dataplasma_model.php */
/* Location: ./application/models/Dataplasma_model.php */