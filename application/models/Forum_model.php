<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forum_model extends CI_Model {

	public function get_data_forum_by_key_user($table_name) {
		return $this->db->get($table_name);
	}

	public function get_data_konten_by_kode($table_name, $kode_key, $field_name) {
		$valid = $this->db->where($field_name, $kode_key);
		$valid = $this->db->get($table_name, 1);
		if ($valid->num_rows() > 0) {
			$this->db->where($field_name, $kode_key);
			return $this->db->get($table_name, 1);
		} else {
			return false;
		}
	} 	

	public function insert_data_forum($table_name, $data) {
		$this->db->insert($table_name, $data);
	}

	public function get_data_kategori($table_name) {
		$this->db->select('kategori, count(kategori) as total_data');
		$this->db->group_by('kategori');
		$this->db->order_by('total_data', 'desc');
		return $this->db->get($table_name);
	}

	public function check_kategori($table_name, $kategori) {
		$valid = $this->db->where('kategori', $kategori);
		$valid = $this->db->get($table_name);
		if ($valid->num_rows() > 0) {
			$this->db->select('*');
			$this->db->where('kategori', $kategori);
			return $this->db->get($table_name);
		} else {
			return false;
		}
	}

	public function check_slug_get_data($table_name, $slug) {
		$valid = $this->db->where('slug_url', $slug);
		$valid = $this->db->get($table_name);
		if ($valid->num_rows() > 0) {
			$this->db->select('*');
			$this->db->where('slug_url', $slug);
			return $this->db->get($table_name, 1);
		} else {
			return false;
		}
	}



	public function update_data_forum($table_name, $kode_key , $field_name, $data) {
		$valid = $this->db->where($field_name, $kode_key);
		$valid = $this->db->get($table_name, 1);

		if ($valid->num_rows() > 0) {
			$this->db->where($field_name, $kode_key);
			$this->db->update($table_name, $data);
			return true;
		} else {
			return false;
		}
	}

	public function delete_data_forum($table_name, $kode_key, $field_name) {
		$valid = $this->db->where($field_name, $kode_key);
		$valid = $this->db->get($table_name, 1);
		if ($valid->num_rows() > 0) {
			$cover_old = $valid->row('file_name');
			if ($cover_old != "default_cover.img") {
				unlink('./storage_img/thubnail/'.$cover_old);
			}

			$this->db->where($field_name, $kode_key);
			$this->db->delete($table_name);
			return true;
		} else {
			return false;
		}
	}

}

/* End of file Forum_model.php */
/* Location: ./application/models/Forum_model.php */