<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register_model extends CI_Model {

	public function view_data_register($table_name) {
		$this->db->select('*');
		return $this->db->get($table_name);
	}		

	public function insert_data_register($table_name, $data) {
		$this->db->insert($table_name, $data);
	}

	public function edit_data_register($table_name, $kode_key, $data) {
		$valid = $this->db->where('kode_register', $kode_key);
		$valid = $this->db->get($table_name, 1)->result();

		if ($valid->num_rows() > 0) {
			$this->db->where('kode_register', $kode_key);
			$this->db->update($table_name, $data);
			return true;
		} else {
			return false;
		}
	}

	public function delete_data_regiter($table_name, $kode_key) {
		$valid = $this->db->where('kode_register', $kode_key);
		$valid = $this->db->get($table_name, 1);
		if ($valid->num_rows() > 0) {
			$this->db->where('kode_register', $kode_key);
			$this->db->delete($table_name);
			return true;
		} else {
			return false;
		}
	}

}

/* End of file Register_model.php */
/* Location: ./application/models/Register_model.php */