<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

	public function get_login($table_name, $username, $password) {
		$result = $this->db->where('username', $username);
		$result = $this->db->where('password', $password);
		$result = $this->db->get($table_name, 1);
		if ($result->num_rows() > 0) {

		} else {

			redirect('/','refresh');
		}
	}

	public function update_data_log($table_name, $key_log, $username, $password) {
		if ($key_log == "login") {
			$this->db->where('user_name', $username);
			$this->db->where('password', md5($password));
			$result = $this->db->get($table_name);
			if ($result->num_rows() > 0) {
				$data = array(
					'Log_time' => date("Y-m-d H:i:s")
				);
				$this->db->where('user_name', $username);
				$this->db->where('password', md5($password));
				$this->db->update($table_name, $data);
				$data_session = array(
					'kode' => $result->row('kode'),
					'user_name' => $result->row('user_name'),
					'nama' => $result->row('nama'),
					'avatar' => $result->row('avatar'),
					'Session_log' => 1
				);
				$this->session->set_userdata($data_session);
				return true;
			} else {
				return false;
			}
		} else if($key_log == "super") {

			$this->db->where('user_name', $username);
			$this->db->where('password', sha1($password));
			$result = $this->db->get($table_name);
			if ($result->num_rows() > 0) {
				$data = array(
					'Log_time' => date("Y-m-d H:i:s")
				);
				$this->db->where('user_name', $username);
				$this->db->where('password', sha1($password));
				$this->db->update($table_name, $data);
				$data_session = array(
					'kode' => $result->row('kode'),
					'user_name' => $result->row('user_name'),
					'nama' => $result->row('nama'),
					'avatar' => $result->row('avatar'),
					'Session_log' => 200
				);
				$this->session->set_userdata($data_session);
				return true;
			} else {
				return false;
			}

		} else if ($key_log == "logout") {
			$this->db->where('user_name', $username);
			$this->db->where('kode', $password);
			$dt_result = $this->db->get($table_name);
			if ($dt_result->num_rows() > 0) {
				$data = array(
				'Out_time' => date("Y-m-d H:i:s")
				);
				$this->db->where('user_name', $username);
				$this->db->where('kode', $password);
				$this->db->update($table_name, $data);
				$this->session->unset_userdata(array('kode'=> '', 'user_name' => '', 'Session_log' => '', 'nama' => '', 'avatar' => ''));
				session_destroy();
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}

	}		
}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */