<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('Dataplasma_model');
		$this->load->model('Lib_model');
		$this->load->model('Register_model');
		$this->load->model('Forum_model');
		$this->load->model('Command_model');

		$this->load->library('user_agent');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index()
	{
		$m_dt_plasma = new Dataplasma_model();

		$week = date('yW')-1;
		$week_last = date('yW')-2;
		$kode = $this->session->userdata('kode');

		$data['data_populasi_001'] = $m_dt_plasma->total_populasi($week, '001')->row();
		$data['data_populasi_003'] = $m_dt_plasma->total_populasi($week, '003')->row();
		$data['data_populasi_008'] = $m_dt_plasma->total_populasi($week, '008')->row();
		$data['data_populasi_005'] = $m_dt_plasma->total_populasi($week, '005')->row();

		$data['data_populasi_last_001'] = $m_dt_plasma->total_populasi($week_last, '001')->row();
		$data['data_populasi_last_003'] = $m_dt_plasma->total_populasi($week_last, '003')->row();
		$data['data_populasi_last_008'] = $m_dt_plasma->total_populasi($week_last, '008')->row();
		$data['data_populasi_last_005'] = $m_dt_plasma->total_populasi($week_last, '005')->row();
		$data['data_periode'] = $week;
		$data['data_periode_last'] = $week_last;

		$this->load->view('frontpages/page/home_view', $data);
	}

	public function profil() {
		$this->load->view('frontpages/page/profil_view');
	}

	public function analisa_usaha() {
		$this->load->view('frontpages/page/analisa_usaha_view');
	}

	public function daftar_guyu() {
		$this->load->view('frontpages/page/daftar_guyubingmana_view');
	}

	public function action_register_member() {
		$m_lib = new Lib_model();
		$m_register = new Register_model();

		$nama 			 = $this->input->post('nama_lengkap');
		$no_hp 			 = $this->input->post('no_hp');
		$email 			 = $this->input->post('email');
		$alamat_rumah 	 = $this->input->post('alamat_rumah');
		$pekerjaan 		 = $this->input->post('pekerjaan');
		$alasan_ternak   = $this->input->post('alasan_ternak');
		$status_ternak   = $this->input->post('status_ternak');

		$valid_data = $this->db->where('nomor_telepon', $no_hp)->get('Tbl_Member')->num_rows();
		if ($valid_data > 0) {
			$this->session->set_flashdata('error_data', '<strong>Ups!! </strong> Nomor ini sudah terdaftar '. $no_hp);
			redirect('daftar_guyu','refresh');
		} else {
			$check_data = $this->db->get('Tbl_Member')->num_rows();
			if ($check_data > 0) {
				$get_id = $this->db->select('MAX(kode_member) AS kode_member')->order_by('kode_member', 'desc')->get('Tbl_Member')->row();
					$kode_member = $m_lib->buatkode($get_id->kode_member, 'MEM-', 7);
				$data_member = array(
					'kode_member' => $kode_member,
					'nama_member' => $nama,
					'nomor_telepon' => $no_hp,
					'email' => $email,
					'alamat' => $alamat_rumah,
					'alasan' => $alasan_ternak,
					'status_ternak' => $status_ternak
				);
				$m_register->insert_data_register('Tbl_Member', $data_member);
				$this->session->set_flashdata('message_data', '<strong>Success</strong> Tunggu untuk beberapa waktu koordinator kami menghubungi anda');
				redirect('daftar_guyu','refresh');
			} else {
				$data_member = array(
					'kode_member' => 'MEM-0000001',
					'nama_member' => $nama,
					'nomor_telepon' => $no_hp,
					'email' => $email,
					'alamat' => $alamat_rumah,
					'alasan' => $alasan_ternak,
					'status_ternak' => $status_ternak
				);
				$m_register->insert_data_register('Tbl_Member', $data_member);
				$this->session->set_flashdata('message_data', '<strong>Success</strong> Tunggu untuk beberapa waktu koordinator kami menghubungi anda');
				redirect('daftar_guyu','refresh');
			}
		}
	}

	public function forum_view() {
		$m_forum = new Forum_model();

		$data['data_kategori'] = $m_forum->get_data_kategori('Tbl_Konten')->result();
		$this->load->view('frontpages/page/forum_view', $data);
	}

	public function list_post($kategori) {
		$m_forum = new Forum_model();
		$m_command = new Command_model();

		$valid = $m_forum->check_kategori('Tbl_Konten', $kategori);
		if ($valid > 0) {
			$data['data_konten'] = $m_forum->check_kategori('Tbl_Konten', $kategori)->result();
			$this->load->view('frontpages/page/list_post_blog_view', $data);
		} else {
			redirect('forum','refresh');
		}
	}

	public function konten_view($slug_url) {
		$m_forum = new Forum_model();
		$m_command = new Command_model();

		$valid = $m_forum->check_slug_get_data('Tbl_Konten', $slug_url);
		if ($valid > 0) {
			$kode_konten = $m_forum->check_slug_get_data('Tbl_Konten', $slug_url)->row('kode_konten');

			$data['data_comment'] = $m_command->get_data_command_by_kode_kontent('Tbl_Comment', $kode_konten)->result();
			$data['detail_forum'] = $m_forum->check_slug_get_data('Tbl_Konten', $slug_url)->row();
			$this->load->view('frontpages/page/detail_post_blog_view', $data);
		} else {
			redirect('forum','refresh');
		}
	}

	public function action_regiter_command() {
		$m_command = new Command_model();

		$kode_konten 	= $this->input->post('kode_konten');
		$nama 		 	= $this->input->post('nama');
		$nomor_telepon  = $this->input->post('nomor_telepon');
		$command 		= $this->input->post('command');

		$data = array(
			'kode_command' => implode('-', str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 30), 6)),
			'kode_konten' => $kode_konten,
			'nama_user' => $nama,
			'nomor_telepon' => $nomor_telepon,
			'isi_command' => $command,
			'tgl_command' => date("Y-m-d H:i:s")
		);

		$valid = $m_command->check_user_command('Tbl_Comment', $kode_konten, $nama, $nomor_telepon);

		if ($valid > 0) {
			// create session
			$m_command->insert_data_command('Tbl_Comment', $data);
			$data_session = array(
				'kode_konten' => $valid->row('kode_konten'),
				'nama_user' => $valid->row('nama_user'),
				'nomor_telepon' => $valid->row('nomor_telepon')
			);
			$this->session->set_userdata($data_session);
			redirect($this->agent->referrer());
		} else {
			$m_command->insert_data_command('Tbl_Comment', $data);

			$data_session = array(
				'kode_konten' => $kode_konten,
				'nama_user' => $nama,
				'nomor_telepon' => $nomor_telepon
			);
			// create session command
			$this->session->set_userdata($data_session);
			redirect($this->agent->referrer());
		}
	}

	public function out_command() {
		$data_session = array(
				'kode_konten' => '',
				'nama_user' => '',
				'nomor_telepon' => ''
			);
		$this->session->unset_userdata($data_session);
		session_destroy();
		redirect($this->agent->referrer());
	}

	public function action_faq() {
		$nama = $this->input->post('nama');
		$nomor_handphone = $this->input->post('nomor_handphone');
		$email = $this->input->post('email');
		$alamat_rumah = $this->input->post('alamat_rumah');
		$pertanyaan = $this->input->post('pertanyaan');
	}

}

/* End of file Front.php */
/* Location: ./application/controllers/Front.php */