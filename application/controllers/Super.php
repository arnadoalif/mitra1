<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Super extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		
		// 001 Nanang Brosot
		// 003 Azis Kalasan
		// 008 Sigit Sumpiuh
		// 005 Rio Wonosobo
		if($this->session->userdata('Session_log') != 200){
			redirect('login');
		}

		$this->load->library('user_agent');
		date_default_timezone_set('Asia/Jakarta');
		
		$this->load->model('Dataplasma_model');
		$this->load->model('Lib_model');
		$this->load->model('Forum_model');
		$this->load->model('Command_model');
	}

	public function index()
	{
		$m_dt_plasma = new Dataplasma_model();

		$week = date('yW')-1;
		$week_last = date('yW')-2;
		$kode = $this->session->userdata('kode');

		$data['data_populasi_001'] = $m_dt_plasma->total_populasi($week, '001')->row();
		$data['data_populasi_003'] = $m_dt_plasma->total_populasi($week, '003')->row();
		$data['data_populasi_008'] = $m_dt_plasma->total_populasi($week, '008')->row();
		$data['data_populasi_005'] = $m_dt_plasma->total_populasi($week, '005')->row();

		$data['data_populasi_last_001'] = $m_dt_plasma->total_populasi($week_last, '001')->row();
		$data['data_populasi_last_003'] = $m_dt_plasma->total_populasi($week_last, '003')->row();
		$data['data_populasi_last_008'] = $m_dt_plasma->total_populasi($week_last, '008')->row();
		$data['data_populasi_last_005'] = $m_dt_plasma->total_populasi($week_last, '005')->row();
		$data['data_periode'] = $week;
		$data['data_periode_last'] = $week_last;

		$this->load->view('adminpages/page/super_admin_view', $data);
	}

	public function super_data_plasma() {
		$m_dt_plasma = new Dataplasma_model();
		$kode = '001';

		$week = date('yW')-1;
		$data['data_plasma'] = $m_dt_plasma->detail_plasma($week, $kode)->result();
		$data['data_periode'] = $week;

		$this->load->view('adminpages/page/super_data_plasma_view', $data);
	}

	public function get_data_plasma() {
		$m_dt_plasma = new Dataplasma_model();

		$week = date('yW')-1;
		$week_last = date('yW')-2;
		$kode = $this->input->post('kode');
		
		$data['data_periode'] = $week;
		$data['data_populasi'] = $m_dt_plasma->total_populasi($week, $kode)->row();
		$data['data_populasi_last'] = $m_dt_plasma->total_populasi($week_last, $kode)->row();
		$data['data_periode_last'] = $week_last;
		$data['data_plasma'] = $m_dt_plasma->detail_plasma($week, $kode)->result();

		$this->load->view('adminpages/page/get_data_pasma_view', $data);
	}


	public function analisa_usaha() {
		$data['data_analisa'] = $this->db->get('Tbl_Analisa')->result();
		$this->load->view('adminpages/page/analisa_usaha_view', $data);
	}

	public function create_forum_view() {
		$m_forum = new Forum_model();

		$data['data_kategori'] = $m_forum->get_data_kategori('Tbl_Konten')->result();
		$this->load->view('adminpages/page/create_forum_view', $data);
	}

	public function view_list_kontent($kategori) {
		$m_forum = new Forum_model();
		$m_command = new Command_model();

		$valid = $m_forum->check_kategori('Tbl_Konten', $kategori);
		if ($valid > 0) {
			$data['data_konten'] = $m_forum->check_kategori('Tbl_Konten', $kategori)->result();
			$this->load->view('adminpages/page/list_kontent_view', $data);
		} else {
			redirect('super/create_forum','refresh');
		}
		
	}

	public function action_create_forum() {
		$m_lib = new Lib_model();
		$m_forum = new Forum_model();

		$judul_topik = $this->input->post('judul_topik');
		$kategori = $this->input->post('kategori_topik');
		$isi_topik = $this->input->post('isi_topik');

		$file_name = $_FILES['thubnail']['name'];
		$file_size = $_FILES['thubnail']['size'];
		$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
		$new_file_name = date('Y').'_'.str_replace('-','_',$m_lib->slug($judul_topik)).'.'.$file_type;

		if ($_FILES['thubnail']['error'] > 0) {

			$data_konten = array(
				'kode_konten' => implode('-', str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 12), 4)),
				'title_konten' => $judul_topik,
				'slug_url' => $m_lib->slug($judul_topik),
				'kategori' => $kategori,
				'desc_konten' => $isi_topik,
				'file_name' => 'default_cover.img',
				'tgl_create' => date("Y-m-d H:i:s") 
			);

			$m_forum->insert_data_forum('Tbl_Konten', $data_konten);

			$this->session->set_flashdata('message_data', '<strong>Success </strong> Topik berhasil di Tambahkan.');
			redirect($this->agent->referrer());

		} else {

			$config['upload_path'] = './storage_img/thubnail/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']  = '*';
			$config['file_name'] = $new_file_name;
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('thubnail')){
				$error = array('error' => $this->upload->display_errors());

				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat upload gambar');
				redirect($this->agent->referrer());
			}
			else{
				$data = array('upload_data' => $this->upload->data());
			}

			$data_konten = array(
				'kode_konten' => implode('-', str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 12), 4)),
				'title_konten' => $judul_topik,
				'slug_url' => $m_lib->slug($judul_topik),
				'kategori' => $kategori,
				'desc_konten' => $isi_topik,
				'file_name' => $new_file_name,
				'tgl_create' => date("Y-m-d H:i:s") 
			);

			$m_forum->insert_data_forum('Tbl_Konten', $data_konten);

			$this->session->set_flashdata('message_data', '<strong>Success </strong> Topik berhasil di Tambahkan.');
			redirect($this->agent->referrer());
		}

	}

	public function edit_data_konten($kode_konten) {
        $m_forum = new Forum_model();

        $valid = $m_forum->get_data_konten_by_kode('Tbl_Konten', $kode_konten, 'kode_konten');
        if ($valid->num_rows() > 0) {
        	$data['dt_konten'] = $m_forum->get_data_konten_by_kode('Tbl_Konten', $kode_konten, 'kode_konten')->row();
        	$this->load->view('adminpages/page/edit_data_konten_view', $data);
        } else {
        	redirect($this->agent->referrer());
        }
	}

	public function action_update_konten() {
		$m_lib = new Lib_model();
		$m_forum = new Forum_model();

		$kode_konten = $this->input->post('kode_konten');
		$judul_topik = $this->input->post('judul_topik');
		$kategori = $this->input->post('kategori_topik');
		$isi_topik = $this->input->post('isi_topik');
		$cover_old = $this->input->post('cover_old');

		$file_name = $_FILES['thubnail']['name'];
		$file_size = $_FILES['thubnail']['size'];
		$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
		$new_file_name = date('Y').'_'.str_replace('-','_',$m_lib->slug($judul_topik)).'.'.$file_type;

		if ($_FILES['thubnail']['error'] > 0) {

			$data_konten = array(
				'title_konten' => $judul_topik,
				'slug_url' => $m_lib->slug($judul_topik),
				'kategori' => $kategori,
				'desc_konten' => $isi_topik,
				'file_name' => $cover_old
			);

			$m_forum->update_data_forum('Tbl_Konten', $kode_konten, 'kode_konten', $data_konten);

			$this->session->set_flashdata('message_data', '<strong>Success </strong> Topik berhasil di Ubah.');
			redirect('super/create_forum','refresh');

		} else {

			$config['upload_path'] = './storage_img/thubnail/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']  = '*';
			$config['file_name'] = $new_file_name;
			
			if ($cover_old != "default_cover.img") {
				unlink('./storage_img/thubnail/'.$cover_old);
			}

			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('thubnail')){
				$error = array('error' => $this->upload->display_errors());

				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat upload gambar');
				redirect($this->agent->referrer());
			}
			else{
				$data = array('upload_data' => $this->upload->data());
			}

			$data_konten = array(
				'title_konten' => $judul_topik,
				'slug_url' => $m_lib->slug($judul_topik),
				'kategori' => $kategori,
				'desc_konten' => $isi_topik,
				'file_name' => $new_file_name
			);

			$m_forum->update_data_forum('Tbl_Konten', $kode_konten, 'kode_konten', $data_konten);

			$this->session->set_flashdata('message_data', '<strong>Success </strong> Topik berhasil di Ubah.');
			redirect('super/create_forum','refresh');
		}
	}

	public function action_delete_konten($kode_konten) {
		$m_forum = new Forum_model();
		$m_command = new Command_model();

		$valid = $m_forum->delete_data_forum('Tbl_Konten', $kode_konten, 'kode_konten');
		if ($valid > 0) {
			$m_command->delete_data_command('Tbl_Comment', $kode_konten, 'kode_konten');
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Topik berhasil di hapus.');
			redirect('super/create_forum','refresh');

		} else {
			$this->session->set_flashdata('error_data', '<strong>Opss </strong> Topik gagal di hapus.');
			redirect('super/create_forum','refresh');
		}

	}

}

/* End of file Super.php */
/* Location: ./application/controllers/Super.php */