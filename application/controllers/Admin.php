<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		// 001 Nanang Brosot
		// 003 Azis Kalasan
		// 008 Sigit Sumpiuh
		// 005 Rio Wonosobo
		if($this->session->userdata('Session_log') != 1){
			redirect('login');
		}
		$this->load->model('Dataplasma_model');
	}

	public function index()
	{
		$m_dt_plasma = new Dataplasma_model();

		$week = date('yW')-1;
		$week_last = date('yW')-2;
		$kode = $this->session->userdata('kode');

		$data['data_populasi'] = $m_dt_plasma->total_populasi($week, $kode)->row();
		$data['data_populasi_last'] = $m_dt_plasma->total_populasi($week_last, $kode)->row();
		$data['data_periode'] = $week;
		$data['data_periode_last'] = $week_last;
		$this->load->view('adminpages/page/home_view', $data);
	}

	public function data_plasma() {
		$m_dt_plasma = new Dataplasma_model();
		$kode = $this->session->userdata('kode');

		$week = date('yW')-1;
		$data['data_plasma'] = $m_dt_plasma->detail_plasma($week, $kode)->result();
		$data['data_periode'] = $week;
		$this->load->view('adminpages/page/data_plasma_view', $data);
	}

	

}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */