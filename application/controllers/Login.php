<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->library('user_agent');
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Login_model');
	}

	public function index()
	{
		$this->load->view('login_view');
	}

	public function action_login() {
		$m_login = new Login_model();
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		if (($username == 'peksi') && (sha1($password) == '5b4880e919b7351a68224a43af4cfae8d3f03a8c')) {
			$result = $m_login->update_data_log('Tbl_Login', 'super', $username, $password);
			if ($result > 0) {
				redirect('super','refresh');
			} else {
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Username and Password Not Valid.');
				redirect($this->agent->referrer());
			}
		} else {
			$result = $m_login->update_data_log('Tbl_Login', 'login', $username, $password);
			if ($result > 0) {
				redirect('admin','refresh');
			} else {
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Username and Password Not Valid.');
				redirect($this->agent->referrer());
			}
		}
	}

	public function logout() {
		$m_login = new Login_model();

		$username = $this->session->userdata('user_name');
		$password = $this->session->userdata('kode');

		$result = $m_login->update_data_log('Tbl_Login', 'logout', $username, $password);
		if ($result > 0) {
			redirect('login','refresh');
		} else {
			redirect('admin','refresh');
		}

	}

}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */